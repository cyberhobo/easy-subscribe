<?php

function es_topic_init() {
	register_post_type( ES_Post_Types::TOPIC, array(
		'hierarchical'        => false,
		'public'              => false,
		'show_in_nav_menus'   => true,
		'show_ui'             => true,
		'supports'            => array( 'title', 'editor', 'author', 'comments', 'custom-fields' ),
		'has_archive'         => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => 'topics' ),
		'labels'              => array(
			'name'                => __( 'Topics', 'EasySubscribe' ),
			'singular_name'       => __( 'Topic', 'EasySubscribe' ),
			'add_new'             => __( 'Add new Topic', 'EasySubscribe' ),
			'all_items'           => __( 'Topics', 'EasySubscribe' ),
			'add_new_item'        => __( 'Add new Topic', 'EasySubscribe' ),
			'edit_item'           => __( 'Edit Topic', 'EasySubscribe' ),
			'new_item'            => __( 'New Topic', 'EasySubscribe' ),
			'view_item'           => __( 'View Topic', 'EasySubscribe' ),
			'search_items'        => __( 'Search Topics', 'EasySubscribe' ),
			'not_found'           => __( 'No Topics found', 'EasySubscribe' ),
			'not_found_in_trash'  => __( 'No Topics found in trash', 'EasySubscribe' ),
			'parent_item_colon'   => __( 'Parent Topic', 'EasySubscribe' ),
			'menu_name'           => __( 'Topics', 'EasySubscribe' ),
		),
	) );

}
add_action( 'init', 'es_topic_init' );

function es_topic_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages[ES_Post_Types::TOPIC] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Topic updated. <a target="_blank" href="%s">View Topic</a>', 'EasySubscribe'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'EasySubscribe'),
		3 => __('Custom field deleted.', 'EasySubscribe'),
		4 => __('Topic updated.', 'EasySubscribe'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Topic restored to revision from %s', 'EasySubscribe'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Topic published. <a href="%s">View Topic</a>', 'EasySubscribe'), esc_url( $permalink ) ),
		7 => __('Topic saved.', 'EasySubscribe'),
		8 => sprintf( __('Topic submitted. <a target="_blank" href="%s">Preview Topic</a>', 'EasySubscribe'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Topic scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Topic</a>', 'EasySubscribe'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Topic draft updated. <a target="_blank" href="%s">Preview Topic</a>', 'EasySubscribe'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'es_topic_updated_messages' );
