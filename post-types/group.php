<?php

function group_init() {
	register_post_type( ES_Post_Types::GROUP, array(
		'hierarchical'        => false,
		'public'              => true,
		'show_in_nav_menus'   => true,
		'show_ui'             => true,
		'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'custom-fields' ),
		'has_archive'         => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => 'groups' ),
		'labels'              => array(
			'name'                => __( 'Groups', 'EasySubscribe' ),
			'singular_name'       => __( 'Group', 'EasySubscribe' ),
			'add_new'             => __( 'Add new group', 'EasySubscribe' ),
			'all_items'           => __( 'Groups', 'EasySubscribe' ),
			'add_new_item'        => __( 'Add new group', 'EasySubscribe' ),
			'edit_item'           => __( 'Edit group', 'EasySubscribe' ),
			'new_item'            => __( 'New group', 'EasySubscribe' ),
			'view_item'           => __( 'View group', 'EasySubscribe' ),
			'search_items'        => __( 'Search groups', 'EasySubscribe' ),
			'not_found'           => __( 'No groups found', 'EasySubscribe' ),
			'not_found_in_trash'  => __( 'No groups found in trash', 'EasySubscribe' ),
			'parent_item_colon'   => __( 'Parent group', 'EasySubscribe' ),
			'menu_name'           => __( 'Groups', 'EasySubscribe' ),
		),
	) );

}
add_action( 'init', 'group_init' );

function group_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages[ES_Post_Types::GROUP] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Group updated. <a target="_blank" href="%s">View group</a>', 'EasySubscribe'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'EasySubscribe'),
		3 => __('Custom field deleted.', 'EasySubscribe'),
		4 => __('Group updated.', 'EasySubscribe'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Group restored to revision from %s', 'EasySubscribe'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Group published. <a href="%s">View group</a>', 'EasySubscribe'), esc_url( $permalink ) ),
		7 => __('Group saved.', 'EasySubscribe'),
		8 => sprintf( __('Group submitted. <a target="_blank" href="%s">Preview group</a>', 'EasySubscribe'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Group scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview group</a>', 'EasySubscribe'),
		// translators: Publish box date format, see http://php.net/date
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Group draft updated. <a target="_blank" href="%s">Preview group</a>', 'EasySubscribe'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'group_updated_messages' );
