<?php

class EasySubscribeAjaxTest extends WP_Ajax_UnitTestCase {

	protected $_author = null;
	protected $_post = null;

	public function setUp() {
		parent::setUp();
		$this->_author = $this->factory->user->create_and_get();
		$this->_post = $this->factory->post->create_and_get( array(
			'post_author' => $this->_author->ID,
		) );
	}

	public function testAsAdministrator() {
		$this->_setRole( 'administrator' );

		$this->successfulSubscriptionCases();
	}

	public function testAsSubscriber() {
		$this->_setRole( 'subscriber' );

		$this->successfulSubscriptionCases();
	}

	private function successfulSubscriptionCases() {
		$subscriptions = array(
			'user' => $this->_author,
			'post' => $this->_post,
		);
		$subscriber = wp_get_current_user();

		foreach ( $subscriptions as $type => $object ) {
			$_POST['subscribe_nonce'] = wp_create_nonce( ES_Ajax::AJAX_NONCE );
			$_POST['meta_type'] = $type;
			$_POST['object_id'] = $object->ID;
			$_POST['subscribe_submit'] = 'subscribe';

			$detected_subscription_email = false;
			$detected_unsubscription_email = false;
			$email_filter = function( $mail ) use ( $subscriber, &$detected_subscription_email, &$detected_unsubscription_email ) {
				if ( $mail['to'] == $subscriber->user_email ) {
					if ( strpos( $mail['subject'], ' subscribed' ) !== false )
						$detected_subscription_email = true;
					else if ( strpos( $mail['subject'], ' unsubscribed' ) !== false )
						$detected_unsubscription_email = true;
				}
				return $mail;
			};
			add_filter( 'wp_mail', $email_filter, 9 );

			try {
				$this->_handleAjax( EasySubscribe::SUBSCRIBE_ACTION );
			} catch ( WPAjaxDieContinueException $e ) {
				unset( $e );
			}

			$this->assertNotEmpty( $this->_last_response );
			$this->assertNotContains( 'Notice:', $this->_last_response );
			$this->assertNotContains( 'Error:', $this->_last_response );
			$this->assertTrue( $detected_subscription_email, 'No subscribed email detected.' );
			$this->assertFalse( $detected_unsubscription_email, 'Unexpected unsubscribed email detected.' );
			$this->assertTrue(
				EasySubscribe::is_subscribed( $type, wp_get_current_user(), $object ),
				'Subscription failed.'
			);

			$_POST['subscribe_submit'] = 'unsubscribe';
			$detected_subscription_email = false;

			try {
				$this->_handleAjax( EasySubscribe::SUBSCRIBE_ACTION );
			} catch ( WPAjaxDieContinueException $e ) {
				unset( $e );
			}

			$this->assertNotEmpty( $this->_last_response );
			$this->assertNotContains( 'Notice:', $this->_last_response );
			$this->assertNotContains( 'Error:', $this->_last_response );
			$this->assertTrue( $detected_unsubscription_email, 'No unsubscribed email detected.' );
			$this->assertFalse( $detected_subscription_email, 'Unexpected subscribed email detected.' );
			$this->assertFalse(
				EasySubscribe::is_subscribed( $type, wp_get_current_user(), $object ),
				'Unsubscription failed.'
			);

			remove_filter( 'wp_mail', $email_filter, 9 );
		}
	}

	public function testNewUser() {
		wp_logout();
		$test_email = 'testuser@easysubscribe.com';
		$_POST['subscribe_nonce'] = wp_create_nonce( ES_Ajax::AJAX_NONCE );
		$_POST['meta_type'] = 'post';
		$_POST['subscribe_email'] = $test_email;
		$_POST['object_id'] = $this->_post->ID;
		$_POST['subscribe_submit'] = 'subscribe';

		$detected_new_user_email = false;
		$new_user_email_filter = function( $mail ) use ( $test_email, &$detected_new_user_email ) {
			if ( $mail['to'] == $test_email and strpos( $mail['subject'], 'username' ) !== false )
				$detected_new_user_email = true;
			return $mail;
		};
		add_filter( 'wp_mail', $new_user_email_filter, 9 );

		try {
			$this->_handleAjax( EasySubscribe::SUBSCRIBE_ACTION );
		} catch ( WPAjaxDieContinueException $e ) {
			unset( $e );
		}

		remove_filter( 'wp_mail', $new_user_email_filter, 9 );

		$this->assertTrue( $detected_new_user_email, 'No new user email detected.' );

		$this->assertNotEmpty( $this->_last_response );
		$this->assertNotContains( 'Notice:', $this->_last_response );
		$this->assertNotContains( 'Error:', $this->_last_response );

		$this->assertCount( 2, EasySubscribe::get_subscriber_ids( 'post', $this->_post ) );
		$this->assertContains( $this->_post->post_author, EasySubscribe::get_subscriber_ids( 'post', $this->_post ) );

		$new_user = get_user_by( 'email', $test_email );
		$this->assertInstanceOf( 'WP_User', $new_user );
		$this->assertContains( $new_user->ID, EasySubscribe::get_subscriber_ids( 'post', $this->_post ) );

		$this->assertEquals( 'testuser', $new_user->display_name );
	}

	public function testBadNonce() {
		$_POST['subscribe_nonce'] = 'wrong';
		$_POST['meta_type'] = 'user';
		$_POST['object_id'] = $this->_author;
		$_POST['subscribe_submit'] = 'subscribe';

		$this->setExpectedException( 'WPAjaxDieStopException', '-1' );
		$this->_handleAjax( EasySubscribe::SUBSCRIBE_ACTION );
	}
}