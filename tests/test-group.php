<?php

class EasySubscribeGroupTest extends WP_UnitTestCase {
	/** @var  WP_User */
	protected $author;
	/** @var  ES_Group */
	protected $group;


	function setUp() {
		parent::setUp();
		EasySubscribe::$options->set( 'enable_groups', true );
		$this->author = $this->factory->user->create_and_get();
		$group_post = $this->factory->post->create_and_get( array(
			'post_type' => ES_Post_Types::GROUP,
			'post_author' => $this->author->ID,
		) );
		$this->group = new ES_Group( $group_post );
	}

	function tearDown() {
		parent::tearDown();
		EasySubscribe::$options->reset();
	}

	function testExistence() {
		$this->assertTrue( class_exists( 'ES_Group' ) );
	}

	function testGroupAuthorSubscription() {
		$topic_author = $this->factory->user->create_and_get();

		$test = $this;
		$email_count = 0;
		$check_email = function( $mail ) use ( $test, &$email_count ) {
			$test->assertEquals( $test->author->user_email, $mail['to'], 'Subscription email not sent to group author.' );
			$test->assertNotContains( 'Notice:', $mail['message'] );
			$test->assertNotContains( 'Error:', $mail['message'] );
			$email_count++;
			return $mail;
		};

		add_filter( 'wp_mail', $check_email );
		$topic_id = $this->group->add_topic( 'group author test topic', 'topic text', $topic_author );
		remove_filter( 'wp_mail', $check_email );

		$this->assertEquals( 1, $email_count, 'Wrong number of new group topic subscription emails sent.' );
		$this->assertTrue(
			EasySubscribe::is_subscribed( 'post', $this->author, $this->group->get_post() ),
			'Group author was not subscribed to group post.'
		);
	}

	function testTopicNotification() {
		EasySubscribe::$options->set( 'enable_groups', true );
		$test_subject = 'test subject';
		$test_body = 'test topic body';
		$subscriber = $this->factory->user->create_and_get();

		$this->group->ensure_subscribed( $subscriber );

		$subscriber_email_detected = false;
		$subscriber_email_filter = function( $mail ) use ( $subscriber, $test_subject, &$subscriber_email_detected ) {
			if ( $mail['to'] == $subscriber->user_email and strpos( $mail['subject'], $test_subject ) >= 0  )
				$subscriber_email_detected = true;
			return $mail;
		};

		add_filter( 'wp_mail', $subscriber_email_filter, 9 );
		$topic_id = $this->group->add_topic( 'test topic', 'topic text', $this->author );
		remove_filter( 'wp_mail', $subscriber_email_filter, 9 );

		$this->assertGreaterThan( 0, $topic_id, 'Invalid topic ID.' );
		$this->assertTrue( $subscriber_email_detected, 'Subscriber was not notified of new topic.' );
	}

	function testAutoCommentNotification() {
		EasySubscribe::$options->set( 'enable_groups', true );
		EasySubscribe::$options->set( 'auto_subscribe_group_comments', true );
		$test_subject = 'test subject';
		$test_body = 'test topic body';
		$subscriber = $this->factory->user->create_and_get();

		$this->group->ensure_subscribed( $subscriber );

		$topic_id = $this->group->add_topic( $test_subject, $test_body, $this->author );
		$this->assertTrue(
			EasySubscribe::is_subscribed( 'post', $subscriber, $topic_id ),
			'Group member was not automatically subscribed to topic comments.'
		);

		$subscriber_email_detected = false;
		$subscriber_email_filter = function( $mail ) use ( $subscriber, &$subscriber_email_detected ) {
			if ( $mail['to'] == $subscriber->user_email )
				$subscriber_email_detected = true;
			return $mail;
		};

		add_filter( 'wp_mail', $subscriber_email_filter, 9 );
		 $this->factory->comment->create( array(
			'comment_post_ID' => $topic_id,
		 ) );
		remove_filter( 'wp_mail', $subscriber_email_filter, 9 );

		$this->assertTrue( $subscriber_email_detected, 'Subscriber was not notified of new topic comment.' );
	}

	function testNewSubscriber() {
		EasySubscribe::$options->set( 'reply_by_email', true );
		EasySubscribe::$options->set( 'reply_by_email_domain', 'example.org' );
		EasySubscribe::$options->set( 'enable_groups', true );
		$subscriber = $this->factory->user->create_and_get();

		$this->group->ensure_subscribed( $subscriber );
		$this->assertTrue( $this->group->is_subscribed( $subscriber ), 'Subscriber was not subscribed.' );

		$test = $this;
		$group = $this->group;
		$email_count = 0;
		$check_email = function( $mail ) use ( $test, $subscriber, $group, &$email_count ) {
			$test->assertEquals( $subscriber->user_email, $mail['to'], 'Wrong to address.' );
			$test->assertNotContains( 'Notice:', $mail['message'], 'Email has a PHP notice.' );
			$test->assertNotContains( 'Error:', $mail['message'], 'Email has a PHP error.' );
			$test->assertContains(
				'reply-to: ' . $group->submission_address( $subscriber ),
				$mail['headers'],
				'Group subscription notice reply-to not group address.'
			);
			$email_count++;
			return $mail;
		};

		add_filter( 'wp_mail', $check_email, 9 );
		ES_Outbound_Handling::send_subscription_notification( 'post', $subscriber, $this->group->get_post() );
		remove_filter( 'wp_mail', $check_email, 9 );

		$this->assertEquals( 1, $email_count, 'Wrong number of emails sent.' );
	}
}

