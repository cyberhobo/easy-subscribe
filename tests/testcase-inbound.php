<?php

class ESInboundDieStopException extends WPDieException {}

class ESInboundDieContinueException extends WPDieException {}

/**
 * Inbound test case class
 *
 * @package    EasySubscribe
 * @subpackage UnitTests
 * @since      0.1
 */
abstract class ES_Inbound_UnitTestCase extends WP_UnitTestCase {

	/**
	 * Last inbound response.  This is set via echo -or- wp_die.
	 * @var type
	 */
	protected $_last_response = '';

	/**
	 * Saved error reporting level
	 * @var int
	 */
	protected $_error_level = 0;

	/**
	 * Set up the test fixture.
	 * Override wp_die(), pretend to be ajax, and suppres E_WARNINGs
	 */
	public function setUp() {
		parent::setUp();

		add_filter( 'wp_die_ajax_handler', array( $this, 'getDieHandler' ), 1, 1 );
		if ( !defined( 'DOING_AJAX' ) )
			define( 'DOING_AJAX', true );

		// Clear logout cookies
		add_action( 'clear_auth_cookie', array( $this, 'logout' ) );

		// Suppress warnings from "Cannot modify header information - headers already sent by"
		$this->_error_level = error_reporting();
		error_reporting( $this->_error_level & ~E_WARNING );
	}

	/**
	 * Tear down the test fixture.
	 * Reset $_POST, remove the wp_die() override, restore error reporting
	 */
	public function tearDown() {
		parent::tearDown();
		$_POST = array();
		$_GET = array();
		unset( $GLOBALS['post'] );
		unset( $GLOBALS['comment'] );
		remove_filter( 'wp_die_ajax_handler', array( $this, 'getDieHandler' ), 1, 1 );
		remove_action( 'clear_auth_cookie', array( $this, 'logout' ) );
		error_reporting( $this->_error_level );
	}

	/**
	 * Clear login cookies, unset the current user
	 */
	public function logout() {
		unset( $GLOBALS['current_user'] );
		$cookies = array(AUTH_COOKIE, SECURE_AUTH_COOKIE, LOGGED_IN_COOKIE, USER_COOKIE, PASS_COOKIE);
		foreach ( $cookies as $c )
			unset( $_COOKIE[$c] );
	}

	/**
	 * Return our callback handler
	 * @return callback
	 */
	public function getDieHandler() {
		return array( $this, 'dieHandler' );
	}

	/**
	 * Handler for wp_die()
	 * Save the output for analysis, stop execution by throwing an exception.
	 * Error conditions (no output, just die) will throw <code>ESInboundDieStopException( $message )</code>
	 * You can test for this with:
	 * <code>
	 * $this->setExpectedException( 'ESInboundDieStopException', 'something contained in $message' );
	 * </code>
	 * Normal program termination (wp_die called at then end of output) will throw <code>ESInboundDieContinueException( $message )</code>
	 * You can test for this with:
	 * <code>
	 * $this->setExpectedException( 'ESInboundDieContinueException', 'something contained in $message' );
	 * </code>
	 * @param string $message
	 * @throws ESInboundDieStopException|ESInboundDieContinueException
	 */
	public function dieHandler( $message ) {
		$this->_last_response .= ob_get_clean();
		ob_end_clean();
		if ( '' === $this->_last_response ) {
			if ( is_scalar( $message) ) {
				throw new ESInboundDieStopException( (string) $message );
			} else {
				throw new ESInboundDieStopException( '0' );
			}
		} else {
			throw new ESInboundDieContinueException( $message );
		}
	}

	/**
	 * Switch between user roles
	 * E.g. administrator, editor, author, contributor, subscriber
	 * @param string $role
	 */
	protected function _setRole( $role ) {
		$post = $_POST;
		$user_id = $this->factory->user->create( array( 'role' => $role ) );
		wp_set_current_user( $user_id );
		$_POST = array_merge($_POST, $post);
	}

	/**
	 * Mimic the ajax handling of admin-ajax.php
	 * Capture the output via output buffering, and if there is any, store
	 * it in $this->_last_message.
	 * @param string $content
	 */
	protected function _handleInbound( $content ) {

		// Start output buffering
		ini_set( 'implicit_flush', false );
		ob_start();

		// Build the request
		$GLOBALS['HTTP_RAW_POST_DATA'] = $content;
		$_GET['es_inbound']  = '1';
		set_query_var( 'es_inbound', 1 );
		$_REQUEST = array_merge( $_POST, $_GET );

		// Call the hooks
		do_action( 'template_redirect', null );

		// Save the output
		$buffer = ob_get_clean();
		if ( !empty( $buffer ) )
			$this->_last_response = $buffer;
		echo $buffer;
	}
}


