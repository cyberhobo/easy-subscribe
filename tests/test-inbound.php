<?php

require_once dirname( __FILE__ ) . '/testcase-inbound.php';

class EasySubscribeInboundTest extends ES_Inbound_UnitTestCase {
	const VALID_IP = '50.31.156.105';
	const INVALID_IP = '66.155.40.250';

	/** @var WP_User */
	protected $_commenter = null;
	/** @var WP_Post */
	protected $_post = null;

	function setUp() {
		parent::setUp();
		$this->_commenter = $this->factory->user->create_and_get();
		$this->_post = $this->factory->post->create_and_get();

		// Too late to override reply_by_email setting, just add hooks
		EasySubscribe::add_inbound_email_hooks();
	}

	function testExistence() {
		$this->assertTrue( class_exists( 'ES_Inbound_Handling' ), 'Inbound handling class doesn\'t exist.' );
	}

	function testInboundComment() {

		$reply_key = new ES_Reply_Key( $this->_post, $this->_commenter );
		$inbound_email = array(
			'MailboxHash' => $reply_key->code,
			'TextBody' => <<<EOD
I'm an email comment. This is my text.

It has a couple of lines. The next text is quoted from the original email.

On Mon, May 13, 2013 at 1:42 PM, Test <test@example.org> wrote:

> [image: Test]
>
> INTRO TEXT
>
> *Dolore twee carles narwhal put a bird on it ut mumblecore, thundercats
> semiotics fixie assumenda. Dolor letterpress pour-over lo-fi. Anim tofu
> echo park, velit nisi eu gluten-free. Direct trade dreamcatcher
> exercitation, four loko in street art voluptate labore dolor leggings wolf
> delectus flexitarian lo-fi mollit. Biodiesel artisan ethical, typewriter
> brooklyn vinyl raw denim pop-up ut messenger bag marfa mustache high life.
> Aute carles etsy do beard, scenester gentrify et excepteur authentic fap
> occupy bicycle rights. Excepteur accusamus minim, deserunt et laboris VHS
> semiotics food truck letterpress mlkshk cliche irony occupy marfa.*
>
>
>    - To *answer*: reply to this email with your answer.
>    - To *view the full conversation* on the web visit
>    http://example.com/post/url/
>
>
>    - To *unsubscribe* from this question: Reply to this email with word
>    \'unsubscribe\'.
>
EOD
		);
		$_SERVER['REMOTE_ADDR'] = self::VALID_IP;

		try {
			$this->_handleInbound( json_encode( $inbound_email ) );
		} catch ( ESInboundDieContinueException $e ) {
			unset( $e );
		}

		$comments = get_approved_comments( $this->_post->ID );
		$this->assertCount( 1, $comments, 'Comment wasn\'t added.' );
		$this->assertEquals( $this->_commenter->ID, $comments[0]->user_id, 'Comment posted from the wrong user.' );
		$this->assertNotContains( 'INTRO TEXT', $comments[0]->comment_content, 'Quoted email not stripped from comment.' );
	}

	function testInboundTopic() {
		$group_post = $this->factory->post->create_and_get( array(
			'post_type' => ES_Post_Types::GROUP,
		) );
		$group = new ES_Group( $group_post );
		$member = $this->factory->user->create_and_get();
		$group->ensure_subscribed( $member );

		$reply_key = new ES_Reply_Key( $group->get_post(), $member );
		$subject = 'Test Topic Title';
		$inbound_email = array(
			'MailboxHash' => $reply_key->code,
			'Subject' => $subject,
			'TextBody' => <<<EOD
I'm an email topic. This is my text. It might have:

<ul>
<li>Some HTML markup</li>
<li>Some characters &amp; entities</li>
</ul>

<em>Capiche?</em>
EOD
		);
		$_SERVER['REMOTE_ADDR'] = self::VALID_IP;

		try {
			$this->_handleInbound( json_encode( $inbound_email ) );
		} catch ( ESInboundDieContinueException $e ) {
			unset( $e );
		}

		$topics = get_posts( array(
			'post_type' => ES_Post_Types::TOPIC,
			'post_parent' => $group->get_post()->ID,
			'numberposts' => -1,
		) );

		$this->assertCount( 1, $topics, 'Topic wasn\'t added.' );
		$this->assertEquals( $member->ID, $topics[0]->post_author, 'Topic posted from the wrong user.' );
		$this->assertEquals( $subject, $topics[0]->post_title, 'Topic title does not include email subject.' );
		$this->assertContains( 'HTML markup', $topics[0]->post_content, 'Email text missing from topic body.' );

		unset( $inbound_email['MailboxHash'] );
		$email_user = 'test';
		EasySubscribe::$options->set( 'reply_by_email_user', $email_user );
		$email_domain = 'domain.com';
		EasySubscribe::$options->set( 'reply_by_email_domain', $email_domain );
		$readable_address = new ES_Readable_Address( $reply_key, ES_Post_Types::GROUP );
		$inbound_email['To'] = $readable_address->get_address();
		$subject2 = 'Second Test Subject';
		$inbound_email['Subject'] = $subject2;

		try {
			$this->_handleInbound( json_encode( $inbound_email ) );
		} catch ( ESInboundDieContinueException $e ) {
			unset( $e );
		}

		$topics = get_posts( array(
			'post_type' => ES_Post_Types::TOPIC,
			'post_parent' => $group->get_post()->ID,
			'numberposts' => -1,
		) );

		$this->assertCount( 2, $topics, 'Topic wasn\'t added.' );
		$this->assertEquals( $member->ID, $topics[0]->post_author, 'Topic posted from the wrong user.' );
		$this->assertEquals( $subject2, $topics[0]->post_title, 'Topic title does not include email subject.' );
		$this->assertContains( 'HTML markup', $topics[0]->post_content, 'Email text missing from topic body.' );
	}

	function testCommands() {
		$reply_key = new ES_Reply_Key( $this->_post, $this->_commenter );
		$_SERVER['REMOTE_ADDR'] = self::VALID_IP;

		// Prevent subscriptions from failed commands
		add_filter( 'es_send_comment_notifications', '__return_false' );

		$email = array(
			'MailboxHash' => $reply_key->code,
			'TextBody' => 'subscribe',
		);
		try {
			$this->_handleInbound( json_encode( $email ) );
		} catch ( ESInboundDieContinueException $e ) {
			unset( $e );
		}

		$this->assertContains(
			$this->_commenter->ID,
			EasySubscribe::get_subscriber_ids( 'post', $this->_post ),
			'subscribe email command failed'
		);

		$email = array(
			'MailboxHash' => $reply_key->code,
			'TextBody' => "\nunsubscribe\n\nthanks a lot\n",
		);
		try {
			$this->_handleInbound( json_encode( $email ) );
		} catch ( ESInboundDieContinueException $e ) {
			unset( $e );
		}

		$this->assertNotContains(
			$this->_commenter->ID,
			EasySubscribe::get_subscriber_ids( 'post', $this->_post ),
			'unsubscribe email command failed'
		);

		$email = array(
			'MailboxHash' => $reply_key->code,
			'TextBody' => "?subscribe",
		);
		try {
			$this->_handleInbound( json_encode( $email ) );
		} catch ( ESInboundDieContinueException $e ) {
			unset( $e );
		}

		$this->assertContains(
			$this->_commenter->ID,
			EasySubscribe::get_subscriber_ids( 'post', $this->_post ),
			'subscribe email command failed with leading nonalpha character'
		);

		remove_filter( 'es_send_comment_notifications', '__return_false' );
	}

	function testBadSource() {
		$reply_key = new ES_Reply_Key( $this->_post, $this->_commenter );
		$email = array(
			'MailboxHash' => $reply_key->code,
			'TextBody' => 'Test Email',
		);
		$_SERVER['REMOTE_ADDR'] = self::INVALID_IP;

		$this->setExpectedException( 'PHPUnit_Framework_Error_Warning', 'rejected inbound email from ' . self::INVALID_IP );
		$this->_handleInbound( json_encode( $email ) );
	}

	/**
	 * @dataProvider autoresponderProvider
	 */
	function testAutoresponder( $headers ) {
		$reply_key = new ES_Reply_Key( $this->_post, $this->_commenter );
		$_SERVER['REMOTE_ADDR'] = self::VALID_IP;

		$email = array(
			'MailboxHash' => $reply_key->code,
			'TextBody' => 'Test Email',
			'Headers' => $headers,
		);
		$this->setExpectedException( 'PHPUnit_Framework_Error_Warning', 'rejected inbound email from auto responder' );
		$this->_handleInbound( json_encode( $email ) );
	}

	function autoresponderProvider() {
		return array(
			array(
				array(
					array(
						'Name' => 'Precedence',
						'Value' => 'junk',
					),
				),
			),
			array(
				array(
					array(
						'Name' => 'X-Autorespond',
						'Value' => 'autorespond',
					),
				),
			),
			array(
				array(
					array(
						'Name' => 'X-Auto-Response-Suppress',
						'Value' => 'suppress',
					),
				),
			),
			array(
				array(
					array(
						'Name' => 'Auto-Submitted',
						'Value' => 'auto-replied',
					),
				),
			),
		);
	}
}