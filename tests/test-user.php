<?php

class EasySubscribeUserTest extends WP_UnitTestCase {

	function testExistence() {
		$this->assertTrue( class_exists( 'ES_User' ) );
	}

	function testGetSubscribedPosts() {
		$es_user = new ES_User( $this->factory->user->create() );

		$post_ids = $this->factory->post->create_many( 3 );

		$es_user->ensure_subscribed( 'post', $post_ids[0] );
		$es_user->ensure_subscribed( 'post', $post_ids[2] );

		$get_post_ids = wp_list_pluck( $es_user->get_subscribed_posts(), 'ID' );
		$this->assertContains( $post_ids[0], $get_post_ids );
		$this->assertNotContains( $post_ids[1], $get_post_ids );
		$this->assertContains( $post_ids[2], $get_post_ids );
	}

	function testDeletion() {
		$user_ids = $this->factory->user->create_many( 2 );

		$post_ids = $this->factory->post->create_many( 2 );

		EasySubscribe::ensure_subscribed( 'post', $user_ids[0], $post_ids[0] );
		EasySubscribe::ensure_subscribed( 'post', $user_ids[0], $post_ids[1] );
		EasySubscribe::ensure_subscribed( 'post', $user_ids[1], $post_ids[0] );
		EasySubscribe::ensure_subscribed( 'post', $user_ids[1], $post_ids[1] );

		wp_delete_user( $user_ids[0] );

		$this->assertNotContains( $user_ids[0], EasySubscribe::get_subscriber_ids( 'post', $post_ids[0] ) );
		$this->assertContains( $user_ids[1], EasySubscribe::get_subscriber_ids( 'post', $post_ids[0] ) );
		$this->assertNotContains( $user_ids[0], EasySubscribe::get_subscriber_ids( 'post', $post_ids[1] ) );
		$this->assertContains( $user_ids[1], EasySubscribe::get_subscriber_ids( 'post', $post_ids[1] ) );
	}

	function testTermForm() {
		EasySubscribe::$options->set( 'enable_taxonomies', array( 'category' ) );
		$user_id = $this->factory->user->create();
		$es_user = new ES_User( $user_id );
		$cat_id = $this->factory->term->create( array( 'taxonomy' => 'category' ) );
		$cat_sig = 'category+' . $cat_id;

		ob_start();
		ES_User::echo_term_form( $user_id );
		$form_html = ob_get_clean();

		$this->assertContains( $cat_sig, $form_html, 'Profile form does not contain term signature.' );

		$this->assertEmpty( $es_user->get_subscribed_term_signatures(), 'New user has subscribed terms.' );

		$_POST[ES_User::SUBSCRIBED_TERM_META_KEY] = array( $cat_sig );
		ES_User::process_term_form( $user_id );
		$this->assertContains( $cat_sig, $es_user->get_subscribed_term_signatures(), 'Term form subscription failed.' );

		unset( $_POST[ES_User::SUBSCRIBED_TERM_META_KEY] );
		ES_User::process_term_form( $user_id );
		$this->assertEmpty( $es_user->get_subscribed_term_signatures(), 'Term form unsubscription failed.' );
	}
}

