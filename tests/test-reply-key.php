<?php

class ES_ReplyKeyTest extends WP_UnitTestCase {

	function testExistence() {
		$this->assertTrue( class_exists( 'ES_Reply_Key' ) );
	}

	function testCodec() {
		$user_id = $this->factory->user->create();
		$post_id = $this->factory->post->create();

		$encoded_key = new ES_Reply_Key( $post_id, $user_id );
		$this->assertEquals( $post_id, $encoded_key->get_post_id() );
		$this->assertEquals( $post_id, $encoded_key->post_id );
		$this->assertEquals( $user_id, $encoded_key->get_user_id() );
		$this->assertEquals( $user_id, $encoded_key->user_id );
		$this->assertNotEmpty( $encoded_key->get_code(), 'Generated an empty code.' );
		$this->assertNotEmpty( $encoded_key->code, 'Code magic getter failed.' );
		$this->assertNotEquals( md5( $user_id . $post_id ), $encoded_key->code, 'Code is too easily spoofed.' );
		$this->assertNotEquals( md5( $post_id . $user_id ), $encoded_key->code, 'Code is too easily spoofed.' );
		$this->assertLessThan( 2000, strlen( $encoded_key->code ), 'Code is too long for an email address.' );

		$decoded_key = new ES_Reply_Key( $encoded_key->code );
		$this->assertEquals( $post_id, $decoded_key->post_id );
		$this->assertEquals( $user_id, $decoded_key->user_id );
	}
}
