<?php

class ES_ReadableAddressTest extends WP_UnitTestCase {

	function testExistence() {
		$this->assertTrue( class_exists( 'ES_Readable_Address' ) );
	}

	function testCodec() {
		$user1_id = $this->factory->user->create();
		$user2_id = $this->factory->user->create();
		$post = $this->factory->post->create_and_get();
		EasySubscribe::$options->set( 'reply_by_email_user', 'test' );

		$reply_key = new ES_Reply_Key( $post, $user1_id );
		$address = new ES_Readable_Address( $reply_key, 'post' );
		$this->assertNotEmpty( $address->get_address(), 'Generated an empty address' );
		$this->assertNotEmpty(
			is_email( $address->get_address() ),
			'Generated an invalid readable email address.'
		);

		$check_address = new ES_Readable_Address( $address->get_address(), 'post' );
		$this->assertNotEmpty( $check_address->get_reply_key(), 'Failed to decode an address to a reply key' );
		$this->assertEquals(
			$address->get_address(),
			$check_address->get_address(),
			'Decoded address differs from original.'
		);

		$check_reply_key = $check_address->get_reply_key();
		$this->assertEquals(
			$post->ID,
			$check_reply_key->get_post_id(),
			'Decoded post id differs from original.'
		);
		$this->assertEquals(
			$user1_id,
			$check_reply_key->get_user_id(),
			'Decoded user id differs from original.'
		);
		$this->assertEquals(
			$reply_key->get_code(),
			$check_reply_key->get_code(),
			'Decoded reply key code differs from original.'
		);

		$reply_key2 = new ES_Reply_Key( $post, $user2_id );
		$address2 = new ES_Readable_Address( $reply_key2, 'post' );
		$this->assertNotEquals(
			$address->get_address(),
			$address2->get_address(),
			'Different users have identical readable addresses.'
		);

		$check_address2 = new ES_Readable_Address( $address2->get_address(), 'post' );
		$this->assertEquals(
			$user2_id,
			$check_address2->get_reply_key()->get_user_id(),
			'Second user readable address user differs from original.'
		);
	}
}
