<?php

class EasySubscribeTest extends WP_UnitTestCase {

	function testExistence() {
		$this->assertTrue( class_exists( 'EasySubscribe' ) );
	}

	function testSubscription() {
		$user1 = $this->factory->user->create_and_get();
		$user2 = $this->factory->user->create_and_get();

		$objects = array(
			'post' => $this->factory->post->create_and_get(),
			'user' => $this->factory->user->create_and_get(),
		);

		foreach( $objects as $type => $object ) {
			$this->assertCount( 0, EasySubscribe::get_subscriber_ids( $type, $object ) );
			$this->assertFalse( EasySubscribe::is_subscribed( $type, $user1, $object ) );
			$this->assertFalse( EasySubscribe::is_subscribed( $type, $user2, $object ) );

			EasySubscribe::ensure_subscribed( $type, $user1, $object );
			$this->assertCount( 1, EasySubscribe::get_subscriber_ids( $type, $object ) );
			$this->assertContains( $user1->ID, EasySubscribe::get_subscriber_ids( $type, $object ) );
			$this->assertTrue( EasySubscribe::is_subscribed( $type, $user1, $object ) );
			$this->assertFalse( EasySubscribe::is_subscribed( $type, $user2, $object ) );

			EasySubscribe::ensure_subscribed( $type, $user2, $object );
			$this->assertCount( 2, EasySubscribe::get_subscriber_ids( $type, $object ) );
			$this->assertContains( $user2->ID, EasySubscribe::get_subscriber_ids( $type, $object ) );
			$this->assertTrue( EasySubscribe::is_subscribed( $type, $user1, $object ) );
			$this->assertTrue( EasySubscribe::is_subscribed( $type, $user2, $object ) );

			EasySubscribe::unsubscribe( $type, $user1, $object );
			$this->assertCount( 1, EasySubscribe::get_subscriber_ids( $type, $object ) );
			$this->assertFalse( EasySubscribe::is_subscribed( $type, $user1, $object ) );
			$this->assertTrue( EasySubscribe::is_subscribed( $type, $user2, $object ) );
		}
	}

	function testAutoAuthorSubscription() {
		EasySubscribe::$options->set( 'auto_subscribe_authors', true );

		$author = $this->factory->user->create_and_get();
		$post = $this->factory->post->create_and_get( array(
			'post_author' => $author->ID,
		) );

		$this->assertTrue( EasySubscribe::is_subscribed( 'post', $author, $post ), 'Author was not auto subscribed.' );
		EasySubscribe::$options->reset();
	}

	function testPostNotification() {
		$author = $this->factory->user->create_and_get();
		$subscriber = $this->factory->user->create_and_get();
		$title = 'Test & Title';
		$post = $this->factory->post->create_and_get( array(
			'post_title' => $title,
			'post_status' => 'draft',
			'post_author' => $author->ID,
		) );

		EasySubscribe::ensure_subscribed( 'user', $subscriber, $author );

		$test = $this;
		$subscriber_email_detected = false;
		$subscriber_email_filter = function( $mail ) use ( $subscriber, $title, $test, &$subscriber_email_detected ) {
			if ( $mail['to'] == $subscriber->user_email ) {
				$subscriber_email_detected = true;
				$test->assertContains( $title, $mail['subject'] );
			}
			return $mail;
		};

		add_filter( 'wp_mail', $subscriber_email_filter, 9 );
		$post->post_status = 'publish';
		wp_update_post( $post );
		remove_filter( 'wp_mail', $subscriber_email_filter, 9 );

		$this->assertTrue( $subscriber_email_detected );
	}

	function testCommentNotification() {
		$subscriber = $this->factory->user->create_and_get();
		$post = $this->factory->post->create_and_get();

		EasySubscribe::ensure_subscribed( 'post', $subscriber, $post );

		$subscriber_email_detected = false;
		$subscriber_email_filter = function( $mail ) use ( $subscriber, &$subscriber_email_detected ) {
			if ( $mail['to'] == $subscriber->user_email )
				$subscriber_email_detected = true;
			return $mail;
		};

		add_filter( 'wp_mail', $subscriber_email_filter, 9 );
		 $this->factory->comment->create( array(
			'comment_post_ID' => $post->ID,
		 ) );
		remove_filter( 'wp_mail', $subscriber_email_filter, 9 );

		$this->assertTrue( $subscriber_email_detected );
	}

	function testUniqueSubscribers() {
		$author_ids = $this->factory->user->create_many( 2 );
		$subscriber_ids = $this->factory->user->create_many( 4 );

		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[0], $author_ids[0] );
		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[1], $author_ids[0] );
		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[1], $author_ids[1] );
		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[2], $author_ids[1] );

		$author_subscribers = EasySubscribe::get_unique_subscriber_ids( 'user' );
		$this->assertCount( 3, $author_subscribers, 'Incorrect author subscriber count.' );
		$this->assertContains( $subscriber_ids[0], $author_subscribers );
		$this->assertContains( $subscriber_ids[1], $author_subscribers );
		$this->assertContains( $subscriber_ids[2], $author_subscribers );

		$post_ids = $this->factory->post->create_many( 2 );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[0], $post_ids[0] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[1], $post_ids[0] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[1], $post_ids[1] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[2], $post_ids[1] );

		$post_subscribers = EasySubscribe::get_unique_subscriber_ids( 'user' );
		$this->assertCount( 3, $post_subscribers, 'Incorrect post subscriber count.' );
		$this->assertContains( $subscriber_ids[0], $post_subscribers );
		$this->assertContains( $subscriber_ids[1], $post_subscribers );
		$this->assertContains( $subscriber_ids[2], $post_subscribers );
	}

	function testGetSubscribedPosts() {
		$post_ids = $this->factory->post->create_many( 4 );
		$subscriber_ids = $this->factory->user->create_many( 2 );

		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[0], $post_ids[1] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[0], $post_ids[3] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[1], $post_ids[0] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[1], $post_ids[2] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[1], $post_ids[3] );

		$subscriber0_post_ids = wp_list_pluck( EasySubscribe::get_subscribed_posts( $subscriber_ids[0] ), 'ID' );
		$this->assertNotContains( $post_ids[0], $subscriber0_post_ids );
		$this->assertContains( $post_ids[1], $subscriber0_post_ids );
		$this->assertNotContains( $post_ids[2], $subscriber0_post_ids );
		$this->assertContains( $post_ids[3], $subscriber0_post_ids );

		$subscriber1_post_ids = wp_list_pluck( EasySubscribe::get_subscribed_posts( $subscriber_ids[1] ), 'ID' );
		$this->assertContains( $post_ids[0], $subscriber1_post_ids );
		$this->assertNotContains( $post_ids[1], $subscriber1_post_ids );
		$this->assertContains( $post_ids[2], $subscriber1_post_ids );
		$this->assertContains( $post_ids[3], $subscriber1_post_ids );
	}

	function testGetSubscribedAuthors() {
		$author_ids = $this->factory->user->create_many( 4 );
		$subscriber_ids = $this->factory->user->create_many( 2 );

		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[0], $author_ids[1] );
		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[0], $author_ids[3] );
		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[1], $author_ids[0] );
		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[1], $author_ids[2] );
		EasySubscribe::ensure_subscribed( 'user', $subscriber_ids[1], $author_ids[3] );

		$subscriber0_post_ids = EasySubscribe::get_subscribed_author_ids( $subscriber_ids[0] );
		$this->assertNotContains( $author_ids[0], $subscriber0_post_ids );
		$this->assertContains( $author_ids[1], $subscriber0_post_ids );
		$this->assertNotContains( $author_ids[2], $subscriber0_post_ids );
		$this->assertContains( $author_ids[3], $subscriber0_post_ids );

		$subscriber1_post_ids = EasySubscribe::get_subscribed_author_ids( $subscriber_ids[1] );
		$this->assertContains( $author_ids[0], $subscriber1_post_ids );
		$this->assertNotContains( $author_ids[1], $subscriber1_post_ids );
		$this->assertContains( $author_ids[2], $subscriber1_post_ids );
		$this->assertContains( $author_ids[3], $subscriber1_post_ids );
	}

	function testSubscriberDeletion() {
		$post_ids = $this->factory->post->create_many( 2 );
		$subscriber_ids = $this->factory->user->create_many( 2 );

		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[0], $post_ids[0] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[0], $post_ids[1] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[1], $post_ids[0] );
		EasySubscribe::ensure_subscribed( 'post', $subscriber_ids[1], $post_ids[1] );

		wp_delete_user( $subscriber_ids[0] );

		$this->assertNotContains( $subscriber_ids[0], EasySubscribe::get_subscriber_ids( 'post', $post_ids[0] ) );
		$this->assertNotContains( $subscriber_ids[0], EasySubscribe::get_subscriber_ids( 'post', $post_ids[1] ) );
		$this->assertContains( $subscriber_ids[1], EasySubscribe::get_subscriber_ids( 'post', $post_ids[0] ) );
		$this->assertContains( $subscriber_ids[1], EasySubscribe::get_subscriber_ids( 'post', $post_ids[1] ) );
	}

	function testTermSubscription() {
		EasySubscribe::$options->set( 'enable_taxonomies', array( 'category', 'post_tag' ) );

		$cat_ids = $this->factory->term->create_many( 2, array( 'taxonomy' => 'category' ) );
		$tag_ids = $this->factory->term->create_many( 2, array( 'taxonomy' => 'post_tag' ) );

		$post_ids = $this->factory->post->create_many( 4 );

		$user_ids = $this->factory->user->create_many( 6 );

		// important that posts with no terms return no subscribers
		$this->assertEmpty( EasySubscribe::get_term_subscriber_ids( array() ) );
		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[0]) );
		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[1]) );
		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[2]) );
		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[3]) );

		wp_set_object_terms( $post_ids[0], $cat_ids[0], 'category' );
		wp_set_object_terms( $post_ids[0], $tag_ids[0], 'post_tag' );
		wp_set_object_terms( $post_ids[1], $cat_ids[0], 'category' );
		wp_set_object_terms( $post_ids[1], $tag_ids[1], 'post_tag' );
		wp_set_object_terms( $post_ids[2], $cat_ids[1], 'category' );
		wp_set_object_terms( $post_ids[2], $tag_ids[0], 'post_tag' );
		wp_set_object_terms( $post_ids[3], $cat_ids[1], 'category' );
		wp_set_object_terms( $post_ids[3], $tag_ids[1], 'post_tag' );

		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[0]) );
		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[1]) );
		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[2]) );
		$this->assertEmpty( EasySubscribe::get_post_term_subscriber_ids( $post_ids[3]) );

		add_user_meta( $user_ids[0], ES_User::SUBSCRIBED_TERM_META_KEY, 'category+' . $cat_ids[0] );
		add_user_meta( $user_ids[0], ES_User::SUBSCRIBED_TERM_META_KEY, 'post_tag+' . $tag_ids[0] );
		add_user_meta( $user_ids[1], ES_User::SUBSCRIBED_TERM_META_KEY, 'category+' . $cat_ids[0] );
		add_user_meta( $user_ids[1], ES_User::SUBSCRIBED_TERM_META_KEY, 'post_tag+' . $tag_ids[1] );
		add_user_meta( $user_ids[2], ES_User::SUBSCRIBED_TERM_META_KEY, 'category+' . $cat_ids[1] );
		add_user_meta( $user_ids[2], ES_User::SUBSCRIBED_TERM_META_KEY, 'post_tag+' . $tag_ids[0] );
		add_user_meta( $user_ids[3], ES_User::SUBSCRIBED_TERM_META_KEY, 'category+' . $cat_ids[1] );
		add_user_meta( $user_ids[3], ES_User::SUBSCRIBED_TERM_META_KEY, 'post_tag+' . $tag_ids[1] );
		add_user_meta( $user_ids[4], ES_User::SUBSCRIBED_TERM_META_KEY, 'category+' . $cat_ids[0] );
		add_user_meta( $user_ids[4], ES_User::SUBSCRIBED_TERM_META_KEY, 'category+' . $cat_ids[1] );
		add_user_meta( $user_ids[4], ES_User::SUBSCRIBED_TERM_META_KEY, 'post_tag+' . $tag_ids[0] );
		add_user_meta( $user_ids[4], ES_User::SUBSCRIBED_TERM_META_KEY, 'post_tag+' . $tag_ids[1] );

		$this->assertCount( 2, EasySubscribe::get_post_term_subscriber_ids( $post_ids[0] ) );
		$this->assertContains( $user_ids[0], EasySubscribe::get_post_term_subscriber_ids( $post_ids[0] ) );
		$this->assertContains( $user_ids[4], EasySubscribe::get_post_term_subscriber_ids( $post_ids[0] ) );
		$this->assertCount( 2, EasySubscribe::get_post_term_subscriber_ids( $post_ids[1] ) );
		$this->assertContains( $user_ids[1], EasySubscribe::get_post_term_subscriber_ids( $post_ids[1] ) );
		$this->assertContains( $user_ids[4], EasySubscribe::get_post_term_subscriber_ids( $post_ids[1] ) );
		$this->assertCount( 2, EasySubscribe::get_post_term_subscriber_ids( $post_ids[2] ) );
		$this->assertContains( $user_ids[2], EasySubscribe::get_post_term_subscriber_ids( $post_ids[2] ) );
		$this->assertContains( $user_ids[4], EasySubscribe::get_post_term_subscriber_ids( $post_ids[2] ) );
		$this->assertCount( 2, EasySubscribe::get_post_term_subscriber_ids( $post_ids[3] ) );
		$this->assertContains( $user_ids[3], EasySubscribe::get_post_term_subscriber_ids( $post_ids[3] ) );
		$this->assertContains( $user_ids[4], EasySubscribe::get_post_term_subscriber_ids( $post_ids[3] ) );
	}

}

