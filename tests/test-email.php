<?php

class EasySubscribeEmailTest extends WP_UnitTestCase {
	private $vcard_string;

	function setUp() {
		parent::setUp();
		$this->vcard_string = "BEGIN:VCARD\r\nN:VCard Tester\r\nEMAIL:test@easysubscribe.org\r\n" .
			"TITLE:Test Vcard\r\nEND:VCARD\r\n";
	}

	function test_email_defaults() {
		$email = new ES_Email( array( 'to' => 'test@easysubscribe.org' ) );
		$this->assertNotEmpty( $email->from_email );
		$this->assertNotEmpty( $email->subject );
		$this->assertEmpty( $email->message );
	}

	function test_text_email() {
		$test_fields = array(
			'to' => 'test@easysubscribe.org',
			'from_name' => 'From Name',
			'from_email' => 'from@email.org',
			'subject' => 'Test Subject',
			'message' => 'Test Message',
			'reply_name' => 'Reply Name',
			'reply_email' => 'reply@email.org',
			'content_type' => 'text/plain',
			'template' => null,
		);
		$email = new ES_Email( $test_fields );
		$email_sent = false;

		$test = $this;
		$check_email = function( $args ) use ( $test, $test_fields, &$email_sent ) {
			$email_sent = true;
			$test->assertEquals( $test_fields['to'], $args['to'] );
			$test->assertEquals( $test_fields['subject'], $args['subject'] );
			$test->assertEquals( $test_fields['message'], $args['message'] );
			$test->assertGreaterThanOrEqual( 2, count( $args['headers'] ) );
			foreach( $args['headers'] as $header ) {
				if ( 'from:' === substr( $header, 0, 5 ) )
					$from = $header;
				else if ( 'reply-to:' === substr( $header, 0, 9 ) )
					$reply = $header;
			}
			$test->assertNotEmpty( $from );
			$test->assertRegExp( '/' . $test_fields['from_email'] . '/', $from );
			$test->assertRegExp( '/' . $test_fields['from_name'] . '/', $from );
			$test->assertNotEmpty( $reply );
			$test->assertRegExp( '/' . $test_fields['reply_email'] . '/', $reply );
			$test->assertRegExp( '/' . $test_fields['reply_name'] . '/', $reply );
			return $args;
		};

		add_filter( 'wp_mail', $check_email );
		$email->send();
		remove_filter( 'wp_mail', $check_email );
		$this->assertTrue( $email_sent, 'Text email was not sent.' );
	}

	function test_email_to_user() {
		$user = $this->factory->user->create_and_get();
		$test_fields = array(
			'to' => $user->user_email,
			'from_name' => 'From Name',
			'from_email' => 'from@email.org',
			'subject' => 'Test Subject',
			'message' => 'Test Message',
			'reply_name' => 'Reply Name',
			'reply_email' => 'reply@email.org'
		);
		$email = new ES_Email( $test_fields );
		$email_sent = false;

		$test = $this;
		$check_email = function( $args ) use ( $test, $test_fields, &$email_sent ) {
			$email_sent = true;
			$test->assertEquals( $test_fields['to'], $args['to'] );
			$test->assertEquals( $test_fields['subject'], $args['subject'] );
			$test->assertContains( $test_fields['message'], $args['message'] );
			$test->assertGreaterThanOrEqual( 2, count( $args['headers'] ) );
			foreach( $args['headers'] as $header ) {
				if ( 'from:' === substr( $header, 0, 5 ) )
					$from = $header;
				else if ( 'reply-to:' === substr( $header, 0, 9 ) )
					$reply = $header;
			}
			$test->assertNotEmpty( $from );
			$test->assertRegExp( '/' . $test_fields['from_email'] . '/', $from );
			$test->assertRegExp( '/' . $test_fields['from_name'] . '/', $from );
			$test->assertNotEmpty( $reply );
			$test->assertRegExp( '/' . $test_fields['reply_email'] . '/', $reply );
			$test->assertRegExp( '/' . $test_fields['reply_name'] . '/', $reply );
			return $args;
		};

		add_filter( 'wp_mail', $check_email );
		$email->send();
		remove_filter( 'wp_mail', $check_email );
		$this->assertTrue( $email_sent, 'Email was not sent.' );
	}

	function test_file_attachments() {
		$file1_name = wp_tempnam();
		$file1_content = 'file1';
		file_put_contents( $file1_name, $file1_content );
		$file2_name = wp_tempnam();
		$file2_content = $this->vcard_string;
		file_put_contents( $file2_name, $file2_content );

		$attachments = array( $file1_name, $file2_name );
		$email = new ES_Email( array(
				'to' => 'to@easysubscribe.org',
				'from_name' => 'From Name',
				'from_email' => 'from@email.org',
				'subject' => 'Attachment Subject',
				'message' => 'Attachment Message',
				'attachments' => $attachments,
			)
		);
		$email_sent = false;

		$test = $this;
		$check_email = function( $args ) use ( $test, $attachments, &$email_sent ) {
			$email_sent = true;
			$test->assertCount( 2, $args['attachments'], 'Wrong number of email attachments.' );
			$test->assertEquals( $attachments, $args['attachments'], 'Email attachments differ from originals.' );
			return $args;
		};

		add_filter( 'wp_mail', $check_email );
		$email->send();
		remove_filter( 'wp_mail', $check_email );
		$this->assertTrue( $email_sent, 'Email was not sent.' );
	}

	function test_string_attachments() {

		$email = new ES_Email( array(
				'to' => 'to@easysubscribe.org',
				'from_name' => 'From Name',
				'from_email' => 'from@email.org',
				'subject' => 'String Attachment Subject',
				'message' => 'String Attachment Message',
			)
		);
		$email_sent = false;

		$string_attachments = array(
			array(
				'string' => 'A text string attachment.',
				'filename' => 'Test content bytes.',
				'encoding' => 'base64',
				'type' => 'application/octet-stream',
			),
			array(
				'string' => $this->vcard_string,
				'filename' => 'test.vcf',
				'encoding' => '8bit',
				'type' => 'text/x-vcard',
			)
		);
		foreach( $string_attachments as $string_attachment ) {
			call_user_func_array( array( $email, 'add_string_attachment' ), $string_attachment );
		}

		$email->send();
		$this->assertFalse( $GLOBALS['phpmailer']->IsError(), 'There was a PHPMailer error.' );
		$this->assertCount( 2, $GLOBALS['phpmailer']->GetAttachments(), 'Outgoing mail had no attachments.' );
	}
}


