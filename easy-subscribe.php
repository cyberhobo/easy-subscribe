<?php
/*
  Plugin Name: Easy Subscribe
  Description: Subscribe an email address to a post or author with minimum hassle.
  Version: 0.4.1
  License: GPL2+
  Author: Dylan Kuhn
  Author URI: http://www.cyberhobo.net/
  Minimum WordPress Version Required: 3.5.1
 */

/*
  Copyright (c) 2013 Dylan Kuhn

  This program is free software; you can redistribute it
  and/or modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation;
  either version 2 of the License, or (at your option) any
  later version.

  This program is distributed in the hope that it will be
  useful, but WITHOUT ANY WARRANTY; without even the implied
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE. See the GNU General Public License for more
  details.
 */

require_once dirname( __FILE__ ) . '/scb/load.php';
require_once dirname( __FILE__ ) . '/core/autoload.php';

EasySubscribe::load();

class EasySubscribe {
	const VERSION = '0.4.1';
	const SUBSCRIBED_META_KEY = 'subscribed_user_ids';
	const SUBSCRIBE_ACTION = 'es_subscribe';
	const INBOUND_QUERY_VAR = 'es_inbound';

	static public $dir_path;
	static public $basename;
	static public $url_path;
	static public $options;

	static private $overridden_options;

	static function load() {
		self::$dir_path = dirname( __FILE__ );
		self::$basename = plugin_basename( __FILE__ );
		self::$url_path = plugins_url( '', __FILE__ );
		load_plugin_textdomain( 'EasySubscribe', '', path_join( dirname( self::$basename ), 'lang' ) );

		ES_Autoload::register( 'ES_', path_join( self::$dir_path, 'core' ) );
		scb_init();

		add_action( 'plugins_loaded', array( __CLASS__, 'action_plugins_loaded' ) );
		add_action( 'widgets_init', array( __CLASS__, 'action_widgets_init' ) );

		add_action( 'transition_post_status', array( 'ES_Outbound_Handling', 'action_transition_post_status' ), 10, 3 );
		add_action( 'wp_insert_comment', array( 'ES_Outbound_Handling', 'action_wp_insert_comment' ), 10, 2 );

		add_action( 'wp_ajax_es_subscribe', array( 'ES_Ajax', 'action_wp_ajax_es_subscribe' ) );
		add_action( 'wp_ajax_nopriv_es_subscribe', array( 'ES_Ajax', 'action_wp_ajax_es_subscribe' ) );

		add_action( 'deleted_user', array( 'ES_User', 'action_deleted_user' ) );
		add_action( 'edit_user_profile', array( 'ES_User', 'echo_term_form' ) );
		add_action( 'show_user_profile', array( 'ES_User', 'echo_term_form' ) );
		add_action( 'edit_user_profile_update', array( 'ES_User', 'process_term_form' ) );
		add_action( 'personal_options_update', array( 'ES_User', 'process_term_form' ) );
	}

	/**
	 * Get the IDs of users subscribed to an object.
	 *
	 * @param string $meta_type Type of object metadata is for (e.g., comment, post, or user)
	 * @param int|object $object The object or object ID.
	 * @return array An array of user IDs.
	 */
	static public function get_subscriber_ids( $meta_type, $object ) {
		$object_id = self::resolve_id( $object );

		$user_ids = get_metadata( $meta_type, $object_id, self::SUBSCRIBED_META_KEY, true );
		if ( !$user_ids )
			$user_ids = array();

		return $user_ids;
	}

	/**
	 * Get the IDs of users subscribed to an author.
	 * @param int|WP_User $author
	 * @return array An array of user IDs.
	 */
	static public function get_author_subscriber_ids( $author ) {
		return self::get_subscriber_ids( 'user', $author );
	}

	/**
	 * Get the IDs of users subscribed to a post.
	 * @param int|WP_Post $post
	 * @return array An array of user IDs.
	 */
	static public function get_post_subscriber_ids( $post ) {
		return self::get_subscriber_ids( 'post', $post );
	}

	/**
	 * Get the IDs of users subscribed to a term or terms.
	 * @param array|object $terms A term object or array of same.
	 * @param string $args Optional arguments:
	 *               relation   AND for subscribers to all terms, OR for subscribers to any term
	 * @return array An array of user IDs.
	 */
	static public function get_term_subscriber_ids( $terms, $args = '' ) {
		$default_args = array( 'relation' => 'AND' );
		$args = wp_parse_args( $args , $default_args );

		if ( empty( $terms ) )
			return array();

		$terms = is_array( $terms ) ? $terms : array( $terms );
		$meta_query = array( 'relation' => $args['relation'] );
		foreach( $terms as $term ) {
			$meta_query[] = array(
				'key' => ES_User::SUBSCRIBED_TERM_META_KEY,
				'value' => $term->taxonomy . '+' . $term->term_id,
			);
		}
		return get_users( array(
			'fields' => 'ids',
			'meta_query' => $meta_query,
		) );
	}

	/**
	 * Get the IDs of users subscribed to the terms in enabled taxonomies for a post.
	 * @param int|WP_Post $post
	 * @return array An array of user IDs.
	 */
	static public function get_post_term_subscriber_ids( $post ) {
		$post_id = self::resolve_id( $post );

		$subscriber_ids = array();
		$taxonomies = self::$options->get( 'enable_taxonomies' );
		if ( !empty( $taxonomies ) ) {
			$terms = wp_get_object_terms( $post_id, $taxonomies );
			$subscriber_ids = self::get_term_subscriber_ids( $terms );
		}
		return apply_filters( 'es_post_term_subscriber_ids', $subscriber_ids, $post_id, $terms );
	}

	/**
	 * Unsubscribe a user from an object.
	 *
	 * @param string $type post or user
	 * @param object|int $user user object or ID to unsubscribe
	 * @param object|int $object object or ID to unsubscribe from.
	 * @return bool True if the user has been unsubscribed.
	 */
	static public function unsubscribe( $type, $user, $object ) {
		$es_user = new ES_User( $user );
		return $es_user->unsubscribe( $type, $object );
	}

	/**
	 * Determine whether a user is subscribed to an object.
	 * 
	 * @param string $type object or user
	 * @param int|object $subscriber
	 * @param int|object $object
	 * @return bool True if the user is subscribed to the object.
	 */
	static public function is_subscribed( $type, $subscriber, $object ) {
		$es_user = new ES_User( $subscriber );
		return $es_user->is_subscribed( $type, $object );
	}

	/**
	 * Ensure that a user is subscribed to an object.
	 *
	 * Does nothing if the user is already subscribed.
	 *
	 * @param string $type object or user
	 * @param int|object $subscriber
	 * @param int|object $object
	 * @return bool True if the user is now subscribed.
	 */
	static public function ensure_subscribed( $type, $subscriber, $object ) {
		$es_user = new ES_User( $subscriber );
		return $es_user->ensure_subscribed( $type, $object );
	}

	/**
	 * Get all subscriber IDs of a type.
	 * @param string $type 'user' or 'post'
	 * @return array
	 */
	static public function get_unique_subscriber_ids( $type ) {
		global $wpdb;

		$subscribers = array();

		if ( 'user' === $type )
			$table = $wpdb->usermeta;
		else if ( 'post' === $type )
			$table = $wpdb->postmeta;
		else
			return $subscribers;

		// TODO: cache
		$sql = $wpdb->prepare( "SELECT meta_value FROM $table WHERE meta_key=%s", self::SUBSCRIBED_META_KEY );
		$results = $wpdb->get_col( $sql );
		if ( !empty( $results ) ) {
			$subscribers = array();
			foreach ( $results as $result ) {
				$subscribers = array_merge( $subscribers, maybe_unserialize( $result ) );
			}
			$subscribers = array_unique( $subscribers );
		}
		return $subscribers;
	}

	/**
	 * Instantiate SCB framework classes.
	 */
	static public function action_plugins_loaded() {
		$default_options = array(
			'reply_by_email' => false,
			'reply_by_email_user' => '',
			'reply_by_email_domain' => 'inbound.postmarkapp.com',
			'auto_subscribe_authors' => true,
			'enable_groups' => false,
			'readable_address' => false,
			'auto_subscribe_group_comments' => false,
			'enable_taxonomies' => array(),
		);
		self::$options = new scbOptions( 'es_options', __FILE__, $default_options );

		$filtered_options = apply_filters( 'es_override_options', array(), self::$options->get() );
		self::$overridden_options = wp_array_slice_assoc( $filtered_options, array_keys( self::$options->get() ) );
		if ( !empty( self::$overridden_options ) )
			self::$options->set( self::$overridden_options );

		if ( self::$options->get( 'reply_by_email' ) and !is_admin() )
			self::add_inbound_email_hooks();

		if ( self::$options->get( 'enable_groups' ) )
			ES_Groups::load();

		if ( is_admin() )
			new ES_Options_Page( __FILE__, self::$options, self::$overridden_options );
	}

	/**
	 * Add hooks needed to catch inbound emails.
	 */
	static public function add_inbound_email_hooks() {
		// Receive webhooks
		add_filter( 'query_vars', array( __CLASS__, 'add_inbound_query_var' ) );
		// Redirect inbound requests before canonical checks
		add_action( 'template_redirect', array( __CLASS__, 'inbound_template_redirect' ), 9 );
	}

	/**
	 * Add the inbound email query variable
	 *
	 * @param $public_query_vars
	 * @return array
	 */
	public static function add_inbound_query_var( $public_query_vars ) {
		$public_query_vars[] = self::INBOUND_QUERY_VAR;
		return $public_query_vars;
	}

	/**
	 * Respond to inbound email requests
	 */
	static public function inbound_template_redirect() {
		// Currently only have one redirect to watch for
		$inbound_request = get_query_var( 'es_inbound' );
		if ( empty( $inbound_request ) )
			return;

		echo ES_Inbound_Handling::process_inbound_email();

		wp_die( '', 'Received inbound email', array( 'response' => 200 ) );
	}

	/**
	 * Search the active theme for a template, falling back to the plugin template.
	 *
	 * Templates are are sought first in a 'easy-subscribe' subdirectory, then the theme
	 * root. If none are found, the plugin default is used.
	 *
	 * @param string $template_name
	 * @return string Selected template.
	 */
	static public function locate_template( $template_name ) {
		$template_names = array(
			'easy-subscribe/' . $template_name,
			$template_name,
		);
		$template = locate_template( $template_names );

		if ( !$template )
			$template = path_join( self::$dir_path, 'templates/' . $template_name );

		return $template;
	}

	/**
	 * Render a template with an array of data in scope.
	 *
	 * @param string $template The template filename
	 * @param array $data An array of data to provide to the template
	 * @param boolean $echo Whether to echo output, default true
	 * @return string Rendered output
	 */
	static public function render_template( $template, $data, $echo = true ) {
		$output = '';
		if ( $template ) {
			extract( $data );
			if ( !$echo )
				ob_start();
			require( $template );
			if ( !$echo )
				$output = ob_get_clean();
		}
		return $output;
	}

	/**
	 * Assemble a reply email address for a user and post.
	 *
	 * If reply by email is not turned on, returns an empty string.
	 *
	 * @param WP_Post $post
	 * @param WP_User $user
	 * @return string Email address if enabled
	 */
	static public function get_reply_address( $post, $user ) {
		if ( !self::$options->get( 'reply_by_email' ) )
			return '';

		$key = new ES_Reply_Key( $post, $user );

		return self::$options->get( 'reply_by_email_user' ) . '+' . $key->code . '@' .
			self::$options->get( 'reply_by_email_domain' );
	}

	/**
	 * Resolve a variable to an ID.
	 *
	 * @param int|string|object $object If an object, return the ID field.
	 * @param string $id_field Optional ID field, 'ID' by default.
	 * @return int The object ID or 0 if not found.
	 */
	static public function resolve_id( $object, $id_field = 'ID' ) {
		return intval( is_object( $object ) ? $object->$id_field : $object );
	}

	/**
	 * Resolve a variable to an object.
	 *
	 * @param string $type post or user
	 * @param string|int|object $object_or_id If not an object, get the appropriate object by id.
	 * @return bool|null|WP_User|WP_Post
	 */
	static public function resolve_object( $type, $object_or_id ) {
		$object = $object_or_id;
		if ( 'post' == $type )
			$object = get_post( $object_or_id );
		else if ( 'user' == $type && !is_object( $object ) )
			$object = get_userdata( $object_or_id );
		return $object;
	}

	/**
	 * Register the subscribe widget.
	 */
	static public function action_widgets_init() {
		require_once self::$dir_path . '/core/widget.php';
		register_widget( 'EasySubscribeWidget' );
	}

	/**
	 * Get the post IDs a user is subscribed to.
	 *
	 * @param int|WP_User $user
	 * @return array posts
	 */
	static public function get_subscribed_posts( $user ) {
		$es_user = new ES_User( $user );
		return $es_user->get_subscribed_posts();
	}

	/**
	 * Get the post IDs a user is subscribed to.
	 *
	 * @param int|WP_User $user
	 * @return array author IDs
	 */
	static public function get_subscribed_author_ids( $user ) {
		$es_user = new ES_User( $user );
		return $es_user->get_subscribed_author_ids();
	}

} // end EasySubscribe class

