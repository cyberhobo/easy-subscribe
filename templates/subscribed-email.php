<?php
/*
 * Template variables in scope:
 * string   $blogname
 * string   $type                   user or post
 * string   $subscribed_to          the user name or post title subscribed to
 * WP_User  $subscriber
 * string   $object                 The thing being subscribed to
 * string   $submission_address     The address to send new submissions to if applicable
 */

if ( 'user' == $type ) {
	$event = 'publishes something';
	$unsubscribe_url = get_author_posts_url( $object->ID );
} else {
	if ( ES_Post_Types::GROUP == $object->post_type )
		$event = 'receives a new post';
	else
		$event = 'receives a new response';
	$unsubscribe_url = get_permalink( $object->ID );
}
?>
You'll receive an email when <?php echo $subscribed_to; ?> <?php echo $event; ?>.

<?php if ( $submission_address ) : ?>
	To post to this group send email to the attached address (<?php echo $submission_address; ?>).
<?php endif; ?>

To unsubscribe visit: <?php echo $unsubscribe_url; ?>

