<?php
/*
 * Template variables in scope:
 * string   $blogname
 * string   $type           user or post
 * string   $subscribed_to  the user name or post title subscribed to
 * WP_User  $subscriber
 * string   $object         The thing being subscribed to
 */

if ( 'user' == $type ) {
	$subscribe_url = get_author_posts_url( $object->ID );
} else {
	$subscribe_url = get_permalink( $object->ID );
}
?>
You'll no longer receive email notices for <?php echo $subscribed_to; ?>.

To re-subscribe visit: <?php echo $subscribe_url; ?>

