<?php
/*
 * Template variables in scope:
 * string   $blogname
 * WP_User  $user       The new user
 * string   $password   The new user plaintext password
 */
?>
Welcome to <?php echo $blogname; ?>. Here is your account information:

Username: <?php echo stripslashes( $user->user_login ); ?>

Password: <?php echo $password; ?>

Login Page: <?php echo wp_login_url(); ?>

