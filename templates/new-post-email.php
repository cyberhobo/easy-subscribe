<?php
/**
 * post notification email template
 * variables in scope:
 * @var {WP_User} $recipient
 * @var {WP_Post} $post
 * @var {WP_User} $author
 */
?>

<p><?php echo $author->display_name; ?> published a new <a href="<?php echo get_permalink( $post ); ?>">post</a>:</p>
<blockquote>
<em><?php echo wpautop( $post->post_content ); ?></em>
</blockquote>

<?php if ( EasySubscribe::$options->get( 'reply_by_email' ) ) : ?>
	<p>To <strong>comment</strong> on this post, reply to this email with your comment.</p>
	<p>To <strong>subscribe</strong> to comments on this post reply with the word 'subscribe'.</p>
<?php endif; ?>
