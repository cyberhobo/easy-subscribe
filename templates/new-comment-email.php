<?php
/**
 * comment notification email template
 * variables in scope:
 * @var {WP_User} $comment_author
 * @var {WP_User} $subscriber
 * @var object $comment
 */
?>

<p><?php echo $comment_author->display_name; ?> made a new <a href="<?php echo get_comment_link(); ?>">comment</a>:</p>
<blockquote>
<em><?php echo wpautop( $comment->comment_content ); ?></em>
</blockquote>

<?php if ( EasySubscribe::$options->get( 'reply_by_email' ) ) : ?>
	<p>To <strong>comment</strong> on this post, reply to this email with your comment.</p>
	<p>To <strong>unsubscribe</strong> from comments on this post reply with the word 'unsubscribe'.</p>
<?php endif; ?>