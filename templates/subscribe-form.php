<?php
/**
 * Variables in scope:
 * EasySubscribeWidget  $widget     The widget generating this form
 * array                $instance   The widget instance data
 * string               $type       'post' or 'user'
 * object               $object     Post or user object of subscription
 * object               $user       Logged in user or null
 * string               $action     'subscribe' or 'unsubscribe'
 */

if ( 'post' == $type and ES_Post_Types::GROUP == $object->post_type and EasySubscribe::is_subscribed( $type, $user, $object ) )
	$group = new ES_Group( $object );
else
	$group = null;
$loading_image_url = path_join( EasySubscribe::$url_path, 'media/ajax-loader.gif' );
?>
<form class="easy-subscribe" method="post">

	<div class="loading-indicator" style="display:none;">
		<img src="<?php echo $loading_image_url; ?>" alt="Loading..." />
	</div>

	<div class="message" style="display:none;"></div>

	<div class="inputs">
		<input id="<?php echo $widget->id; ?>-nonce" name="subscribe_nonce" type="hidden" />

		<input id="<?php echo $widget->id; ?>-action" name="action" type="hidden" value="<?php echo EasySubscribe::SUBSCRIBE_ACTION; ?>" />

		<input id="<?php echo $widget->id; ?>-type" name="meta_type" type="hidden" value="<?php echo $type; ?>" />

		<input id="<?php echo $widget->id; ?>-object-id" name="object_id" type="hidden" value="<?php echo $object->ID; ?>" />


		<?php if ( empty( $user->ID ) ) : ?>

			<input id="<?php echo $widget->id; ?>-email" name="subscribe_email" type="text" value="enter email address" />

		<?php elseif( $group ) : ?>

			<p><?php _e( 'Email this group at ', 'EasySubscribe' ); ?>
				<a href="mailto:<?php echo $group->get_personal_address( $user ); ?>"><?php _e( 'this address', 'EasySubscribe' ); ?></a>
			</p>

		<?php endif; ?>

		<input id="<?php echo $widget->id; ?>-submit" name="subscribe_submit" class="submit" type="submit" value="<?php echo $action; ?>" />
	</div>

</form>
