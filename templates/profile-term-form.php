<?php
/**
 * @var array $taxonomies The taxonomy slugs enabled
 * @var WP_User $user The subscriber
 * @var array $subscribed_term_signatures The term signatures (taxonomy+term_id) the user is subscribed to
 */
$tax_objects = array_map( 'get_taxonomy', $taxonomies );

?>
<h2>Term Subscriptions</h2>
<?php foreach ( $tax_objects as $tax_object ) : ?>
	<div class="taxonomy-subscriptions">
		<?php $terms = get_terms( $tax_object->name, array( 'hide_empty' => false ) ); ?>
		<?php if ( !empty( $terms ) ) : ?>

			<h3><?php echo $tax_object->label; ?></h3>

			<ul>
				<?php foreach ( $terms as $term ) : ?>
					<?php $term_sig = $tax_object->name . '+' . $term->term_id; ?>
					<?php $id = 'es-term-sub-' . $tax_object->name . '-' . $term->term_id; ?>
					<li>
						<input id="<?php echo $id; ?>"
						       name="<?php echo ES_User::SUBSCRIBED_TERM_META_KEY; ?>[]"
						       type="checkbox"
						       <?php checked( in_array( $term_sig, $subscribed_term_signatures ) ); ?>
						       value="<?php echo $term_sig; ?>" />
						<label for="<?php echo $id; ?>"><?php echo $term->name; ?></label>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
<?php endforeach; ?>