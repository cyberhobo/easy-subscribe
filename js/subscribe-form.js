/**global jQuery, es_subscribe_form_env */
jQuery( function( $ ) {
	var $form = $( 'form.easy-subscribe' ),
		$message = $form.find( '.message' ),
		$inputs = $form.find( '.inputs' ),
		$loading_indicator = $form.find( '.loading-indicator' ),
		$nonce_input = $form.find( 'input[name=subscribe_nonce]' ),
		$email_input = $form.find( 'input[name=subscribe_email]' ),
		email_prompt_cleared = false;

	$nonce_input.val( es_subscribe_form_env.nonce );

	$email_input.focus( function() {
		if ( !email_prompt_cleared ) {
			$email_input.val( '' );
			email_prompt_cleared = true;
		}
	} );

	$form.submit( function() {

		$loading_indicator.show();
		$inputs.hide();
		$message.hide();

		$.post( es_subscribe_form_env.ajaxurl, $form.serialize(), function( message ) {

			$message.html( message ).show();
			$loading_indicator.hide();

		} ).error( function() {

			$message.html( es_subscribe_form_env.ajax_error_message ).show();
			$inputs.show();
			$loading_indicator.hide();

		} );
		return false;
	} );
} ); 
