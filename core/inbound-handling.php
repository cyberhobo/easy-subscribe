<?php

class ES_Inbound_Handling {
	const QUERY_VAR = 'es_inbound';

	static private $inbound_ip_white_list = array(
		'50.31.156.6',
		'50.31.156.98',
		'50.31.156.105',
		'50.31.156.106',
		'50.31.156.107',
		'50.31.156.108',
		'50.31.156.109',
	);

	static public function strip_quoted_text( $text ) {
		// Strip text after some kind of ABOVE THIS LINE mark?

		// Remove coded email addresses
		$text = preg_replace( '/<+\s*[+=\/[:alnum:]]*@' . EasySubscribe::$options->get( 'reply_by_email_domain' ) . '\s*>+/', '', $text );

		// Remove google-style quoted replies
		$text = preg_replace( '/\n(>[^\n]*\n){1,}[>\s].*/s', '', $text );

		// Remove any trailing quote introduction (last lines including a year and ending with a colon)
		$text = preg_replace( '/\n[^\n]*' . date( 'Y' ) . '.*:[\s\n\r].*/s', '', $text );
		return $text;
	}

	static public function process_inbound_email() {
		/* Verify origin (Postmark) */
		if ( !in_array( $_SERVER['REMOTE_ADDR'], self::$inbound_ip_white_list ) ) {
			trigger_error( 'rejected inbound email from ' . $_SERVER['REMOTE_ADDR'], E_USER_WARNING );
			return;
		}

		/* Get the post body? http://stackoverflow.com/questions/8391302/json-post-request-parsing-in-php */
		$input = file_get_contents('php://input');
		if ( !$input ) {
			$input = $GLOBALS['HTTP_RAW_POST_DATA'];
			if ( !$input ) {
				trigger_error( 'empty inbound email', E_USER_NOTICE );
				return;
			}
		}
		$input = json_decode( $input );

		if ( self::is_auto_reply( $input ) ) {
			trigger_error( 'rejected inbound email from auto responder', E_USER_WARNING );
			return;
		}

		if ( !empty( $input->MailboxHash ) ) {

			$key = new ES_Reply_Key( $input->MailboxHash );

		} else {

			if ( !isset( $input->To ) ) {
				trigger_error( 'inbound email missing to address', E_USER_NOTICE );
				return;
			}

			$code = self::extract_address_code( $input->To );
			if ( $code ) {
				$key = new ES_Reply_Key( $code );
			}  else {
				$readable_address = new ES_Readable_Address( $input->To );
				$key = $readable_address->get_reply_key();
			}

		}

		if ( !$key->user_id )
			return;

		$text = self::strip_quoted_text( $input->TextBody );

		if ( ES_Post_Types::GROUP == $key->post->post_type ) {
			$group = new ES_Group( $key->post_id );
			return $group->add_topic( $input->Subject, $text, $key->user_id );
		}

		$result = self::do_email_command( $key, $text );
		if ( !$result )
			$result =  self::add_email_comment( $key, $text );

		return $result;
	}

	static private function do_email_command( $key, $text ) {
		$is_command = false;

		if ( preg_match( '/^[[:^alpha:]]*(subscribe|unsubscribe)/', $text, $matches ) ) {

			$is_command = true;

			switch( $matches[1] ) {
				case 'subscribe':
					if ( EasySubscribe::ensure_subscribed( 'post', $key->user, $key->post ) )
						ES_Outbound_Handling::send_subscription_notification( 'post', $key->user, $key->post );
					break;

				case 'unsubscribe':
					if ( EasySubscribe::unsubscribe( 'post', $key->user, $key->post ) )
						ES_Outbound_Handling::send_subscription_notification( 'post', $key->user, $key->post, true );
					break;
			}
		}

		return $is_command;
	}

	static private function add_email_comment( $key, $text ) {
		$comment_data = array(
			'user_id' => $key->user_id,
			'comment_post_ID' => $key->post_id,
			'comment_content' => $text,
		);
		if ( 1 == get_option( 'comment_moderation' ) )
			$comment_data['comment_approved'] = 0;
		else
			$comment_data['comment_approved'] = 1;
		$comment_data = wp_filter_comment( $comment_data );

		if ( self::comment_exists( $key->post_id, $key->user_id, $text ) ) {
			$message = sprintf( __( 'rejected duplicate comment on %s', 'EasySubscribe' ), $key->post_id );
			trigger_error( $message, E_USER_NOTICE );
			return false;
		}

		return wp_insert_comment( $comment_data );
	}

	static private function comment_exists( $post_id, $user_id, $text ) {
		$exists = false;
		$check_comments = get_comments( array(
			'user_id' => $user_id,
			'post_ID' => $post_id,
		) );
		foreach ( $check_comments as $comment ) {
			if ( $comment->comment_content == $text )
				$exists = true;
		}
		return $exists;
	}

	/**
	 * Extract a key from an address if there is one.
	 * @param $address
	 * @return bool|string The key or false if none found.
	 */
	static public function extract_address_code( $address ) {
		$parts = explode( '+', $address );
		if ( count( $parts ) != 2 )
			return false;

		$sub_parts = explode( '@', $parts[1] );
		return $sub_parts[0];
	}

	static protected function is_auto_reply( $message ) {
		$auto_headers = array(
			'Precedence' => 'junk',
			'X-Autorespond' => null,
			'X-Auto-Response-Suppress' => null,
			'Auto-Submitted' => null,
		);

		foreach ( $auto_headers as $name => $value ) {
			foreach ( $message->Headers as $header ) {
				if ( $name == $header->Name ) {
					if ( is_null( $value ) or $value == $header->Value )
						return true;
				}
			}
		}
		return false;
	}

}
