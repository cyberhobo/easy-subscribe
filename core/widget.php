<?php

class EasySubscribeWidget extends WP_Widget {

	// Construct Widget
	function __construct() {
		$default_options = array(
			'description' => __( 'Subscribe a user or email address to a user or post with minimal fuss.', 'EasySubscribe' )
		);
		parent::__construct( false, __( 'Quick Subscribe', 'EasySubscribe' ), $default_options );
	}

	// Display Widget
	function widget( $args, $instance ) {

		wp_enqueue_script( 'es-subscribe-form', path_join( EasySubscribe::$url_path, 'js/subscribe-form.js' ), array( 'jquery' ), EasySubscribe::VERSION, true );
		wp_localize_script( 'es-subscribe-form', 'es_subscribe_form_env', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'spinner_url' => path_join( EasySubscribe::$url_path, 'media/ajax-loader.gif' ),
			'nonce' => wp_create_nonce( ES_Ajax::AJAX_NONCE ),
			'ajax_error_message' => __( 'Sorry, there was a problem reaching the server', 'EasySubscribe' ),
		) );

		/** @var string $before_widget */
		/** @var string $before_title */
		/** @var string $after_title */
		/** @var string $after_widget */
		extract( $args );

		$user = wp_get_current_user();

		$object = isset( $instance['object'] ) ? $instance['object'] : get_queried_object();
		$object = apply_filters( 'es_subscribe_widget_object', $object, $this, $instance );
		if ( is_a( $object, 'WP_Post' ) )
			$type = 'post';
		else if ( is_a( $object, 'WP_User' ) )
			$type = 'user';
		else
			return;

		$submit_address = null;
		if ( $user and EasySubscribe::is_subscribed( $type, $user, $object ) ) {
			if ( 'post' == $type and ES_Post_Types::GROUP == $object->post_type ) {
				if ( EasySubscribe::$options->get( 'readable_address' ) ) {
					$reply_key = new ES_Reply_Key( $object, $user );
					$readable_address = new ES_Readable_Address( $reply_key );
					$submit_address = $readable_address->get_address();
				} else {
					$submit_address = EasySubscribe::get_reply_address( $object, $user );
				}
			}
			$action = 'unsubscribe';
		} else {
			$action = 'subscribe';
		}

		$widget = $this;
		$template_data = compact( 'widget', 'instance', 'user', 'object', 'type', 'action', 'submit_address' );

		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget . $before_title . $title . $after_title;

		$template = EasySubscribe::locate_template( 'subscribe-form.php' );

		EasySubscribe::render_template( $template, $template_data );

		echo $after_widget;
	}

	// Update Widget
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = sanitize_text_field( $new_instance['title'] );

		return $instance;
	}

	// Default value logic
	function get_default_value( $instance, $field, $fallback = '', $escape_callback = 'esc_attr' ) {
		if ( isset( $instance[$field] ) )
			$value = $instance[$field];
		else
			$value = $fallback;

		if ( function_exists( $escape_callback ) )
			$value = call_user_func( $escape_callback, $value );

		return $value;
	}

	// Display Widget Control
	function form( $instance ) {
?>
<p>
	<label for="<?php echo $this->get_field_id( 'title' ); ?>"
		 title="<?php _e( 'Widget heading, leave blank to omit.', 'EasySubscribe' ); ?>">
		 <?php _e( 'Title:', 'EasySubscribe' ); ?>
		<span class="help-tip">?</span>
		<input class="widefat"
			 id="<?php echo $this->get_field_id( 'title' ); ?>"
			 name="<?php echo $this->get_field_name( 'title' ); ?>"
			 type="text"
			 value="<?php echo $this->get_default_value( $instance, 'title' ); ?>" />
	</label>
</p>
<?php
	 }

 }