<?php

/**
 * Take care of sending emails.
 */
class ES_Outbound_Handling {

	/**
	 * Send post subscribers a new comment notification.
	 *
	 * @param object $comment
	 * @param array $subscriber_ids
	 * @param string $template
	 */
	public static function send_comment_notifications( $comment, $subscriber_ids, $template ) {

		if ( !apply_filters( 'es_send_comment_notifications', true, $comment, $subscriber_ids ) )
			return;

		// Turn off native comment notifications
		add_filter( 'comment_notification_recipients', create_function( '$a', 'return array();' ) );

		$comment_author = get_userdata( $comment->user_id );
		$comment_post = get_post( $comment->comment_post_ID );

		if ( ES_Post_Types::TOPIC == $comment_post->post_type ) {
			$group = get_post( $comment_post->post_parent );
			$from_name = $group->post_title;
		} else {
			$group = null;
			$from_name = $comment_author->display_name;
		}

		foreach ( $subscriber_ids as $subscriber_id ) {

			if ( $subscriber_id == $comment->user_id )
				continue;

			$subscriber = get_userdata( $subscriber_id );
			if ( !$subscriber )
				continue;

			$template_data = array(
				'comment_author' => $comment_author,
				'subscriber' => $subscriber,
				'comment' => $comment,
				'comment_post' => $comment_post,
				'group' => $group,
			);

			$encoded_subject = '[' . get_option( 'blogname' ) . '] New comment on "' . $comment_post->post_title . '"';
			$mail = new ES_Email( array(
				'to' => $subscriber->user_email,
				'from_name' => $from_name,
				'reply_email' => EasySubscribe::get_reply_address( $comment_post, $subscriber ),
				'subject' => html_entity_decode( $encoded_subject, ENT_QUOTES, 'UTF-8' ),
				'message' => EasySubscribe::render_template( $template, $template_data, false ),
			) );
			do_action( 'es_comment_email', $mail, $template_data );
			$mail->send();
		}

		EasySubscribe::ensure_subscribed( 'post', $comment_author->ID, $comment_post );
	}

	/**
	 * Send an email to a user who has had an account created for them.
	 *
	 * @param object $user
	 * @param string $password
	 * @param string $template
	 */
	public static function send_new_user_notification( $user, $password, $template ) {
		$blogname = get_option( 'blogname' );

		$template_data = compact( 'blogname', 'user', 'password' );

		$encoded_subject = sprintf( __( '[%s] Your username and password', 'EasySubscribe' ), $blogname );
		$mail = new ES_Email( array(
			'to' => $user->user_email,
			'subject' => html_entity_decode( $encoded_subject, ENT_QUOTES, 'UTF-8' ),
			'message' => EasySubscribe::render_template( $template, $template_data, false ),
		) );
		do_action( 'es_new_user_email', $mail, $template_data );
		$mail->send();
	}

	/**
	 * Send email notifications for a post.
	 * @param object $post
	 * @param array $user_ids
	 * @param string $template The template file to use for the email.
	 */
	public static function send_post_notifications( $post, $user_ids, $template ) {

		if ( !apply_filters( 'es_send_post_notifications', true, $post, $user_ids ) )
			return;

		$author = get_userdata( $post->post_author );
		if ( ES_Post_Types::TOPIC == $post->post_type ) {
			$group = get_post( $post->post_parent );
			$from_name = $group->post_title;
		} else {
			$group = null;
			$from_name = $author->display_name;
		}

		foreach ( $user_ids as $user_id ) {
			$user = get_userdata( $user_id );

			if ( !is_email( $user->user_email ) )
				continue;

			$template_data = array(
				'author' => $author,
				'recipient' => $user,
				'post' => $post,
				'group' => $group,
			);

			$encoded_subject = '[' . get_option( 'blogname' ) . '] ' . $post->post_title;
			$mail = new ES_Email( array(
				'to' => $user->user_email,
				'subject' => html_entity_decode( $encoded_subject, ENT_QUOTES, 'UTF-8' ),
				'from_name' => $from_name,
				'reply_email' => EasySubscribe::get_reply_address( $post, $user ),
				'message' => EasySubscribe::render_template( $template, $template_data, false ),
			) );
			do_action( 'es_post_email', $mail, $template_data );
			$mail->send();
		}
	}

	/**
	 * Send a subscription confirmation email.
	 *
	 * @param string $type post or user
	 * @param int|string|WP_User $subscriber
	 * @param int|string|WP_Post|WP_User $object
	 * @param boolean $un True if unsubscribing, default false.
	 * @return boolean Whether a notification was sent
	 */
	public static function send_subscription_notification( $type, $subscriber, $object, $un = false ) {

		if ( !apply_filters( 'es_send_subscription_notifications', true ) )
			return false;

		$subscriber = EasySubscribe::resolve_object( 'user', $subscriber );

		$object = EasySubscribe::resolve_object( $type, $object );

		$group = ( ES_Post_Types::GROUP == $object->post_type ) ? new ES_Group( $object ) : null;

		$blogname = get_option( 'blogname' );

		$subscribed_to = ( 'post' == $type ) ? $object->post_title : $object->display_name;

		$submission_address = $group ? $group->submission_address( $subscriber ) : null;

		$mail = new ES_Email( array( 'to' => $subscriber->user_email ) );

		if ( $un ) {
			$mail->subject = sprintf( __( '[%s] You\'re unsubscribed from %s', 'EasySubscribe' ), $blogname, $subscribed_to );
			$template = 'unsubscribed-email.php';
			$action = 'es_unsubscribed_email';
		} else {
			$mail->subject = sprintf( __( '[%s] You\'re subscribed to %s', 'EasySubscribe' ), $blogname, $subscribed_to );
			$template = 'subscribed-email.php';
			$action = 'es_subscribed_email';
		}

		$template = EasySubscribe::locate_template( $template );
		$template_data = compact( 'blogname', 'type', 'subscriber', 'object', 'subscribed_to', 'submission_address' );

		$mail->message = EasySubscribe::render_template( $template, $template_data, false );

		if ( $submission_address ) {
			$vcard = sprintf( "BEGIN:VCARD\r\nN:%s\r\nEMAIL:%s\r\nTITLE:%s\r\nEND:VCARD\r\n",
				$object->post_title,
				$submission_address,
				$blogname
			);
			$mail->add_string_attachment( $vcard, $object->post_name . '.vcf', '8bit', 'text/x-vcard' );
			$mail->reply_email = $submission_address;
		}

		do_action( $action, $mail, $template_data );

		return $mail->send();
	}

	/**
	 * Any time a post is published, notify author subscribers.
	 *
	 * @param string $new_status
	 * @param string $old_status
	 * @param WP_Post $post
	 */
	public static function action_transition_post_status( $new_status, $old_status, $post ) {

		if ( empty( $post->post_author ) )
			return;

		if ( 'publish' == $old_status or 'publish' != $new_status )
			return;

		$template = EasySubscribe::locate_template( 'new-post-email.php' );

		$subscriber_ids = null;

		if ( EasySubscribe::$options->get( 'enable_groups' ) ) {
			if ( ES_Post_Types::GROUP == $post->post_type ) {
				// Always subscribe group authors to the group
				EasySubscribe::ensure_subscribed( 'post', $post->post_author, $post );
				return;
			}

			// Subscribers are group members, where the group is the parent post
			if ( ES_Post_Types::TOPIC == $post->post_type )
				$subscriber_ids = EasySubscribe::get_subscriber_ids( 'post', $post->post_parent );
		}

		if ( EasySubscribe::$options->get( 'auto_subscribe_authors' ) )
			EasySubscribe::ensure_subscribed( 'post', $post->post_author, $post );

		if ( !$subscriber_ids ) {
			// Subscribers are subscribed to the post author
			$subscriber_ids = EasySubscribe::get_subscriber_ids( 'user', $post->post_author );

			// Or to post terms
			$post_term_subscriber_ids = EasySubscribe::get_post_term_subscriber_ids( $post );
			$subscriber_ids = array_unique( array_merge( $subscriber_ids, $post_term_subscriber_ids ) );
		}

		self::send_post_notifications( $post, $subscriber_ids, $template );
	}

	/**
	 * When a comment is published notify subscribers if needed.
	 *
	 * @param int $id
	 * @param object $comment
	 */
	public static function action_wp_insert_comment( $id, $comment ) {
		if ( $comment->comment_approved != '1' )
			return;

		$template = EasySubscribe::locate_template( 'new-comment-email.php' );

		self::send_comment_notifications( $comment, EasySubscribe::get_subscriber_ids( 'post', $comment->comment_post_ID ), $template );
	}
}
