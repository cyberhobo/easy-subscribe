<?php

/**
 * Add EasySubscribe group functionality to the native WordPress post API.
 */
class ES_Group {
	private $_id;
	private $_post;

	/**
	 * Create a group based on a post, post ID.
	 * @param int|WP_Post $post
	 */
	public function __construct( $post ) {
		if ( is_object( $post ) ) {
			$this->_id = $post->ID;
			$this->_post = $post;
		} else {
			$this->_id = intval( $post );
		}
	}

	/**
	 * Allow property style access to getters.
	 * @param $name
	 * @return mixed
	 */
	public function __get( $name ) {
		return call_user_func( array( $this, 'get_' . $name ) );
	}

	/**
	 * Get the underlying group post.
	 * @return null|WP_Post
	 */
	public function get_post() {
		if ( !isset( $this->_post ) )
			$this->_post = get_post( $this->_id );
		return $this->_post;
	}

	public function ensure_subscribed( $user ) {
		EasySubscribe::ensure_subscribed( 'post', $user, $this->_id );
	}

	public function unsubscribe( $user ) {
		EasySubscribe::unsubscribe( 'post', $user, $this->_id );
	}

	public function is_subscribed( $user ) {
		return EasySubscribe::is_subscribed( 'post', $user, $this->_id );
	}

	public function get_subscriber_ids() {
		return EasySubscribe::get_subscriber_ids( 'post', $this->_id );
	}

	public function add_topic( $title, $body, $author ) {
		$author_id = is_object( $author ) ? $author->ID : intval( $author );
		$topic_id = wp_insert_post( array(
			'post_title' => $title,
			'post_content' => $body,
			'post_author' => $author_id,
			'post_type' => ES_Post_Types::TOPIC,
			'post_parent' => $this->_id,
			'post_status' => 'draft',
		) );

		if ( is_wp_error( $topic_id ) ) {
			trigger_error( 'Failed to create topic: ' . $topic_id->get_error_message(), E_USER_WARNING );
			return $topic_id;
		}

		if ( EasySubscribe::$options->get( 'auto_subscribe_group_comments' ) ) {
			$group_subscriber_ids = EasySubscribe::get_subscriber_ids( 'post', $this->_id );
			foreach( $group_subscriber_ids as $subscriber_id ) {
				EasySubscribe::ensure_subscribed( 'post', $subscriber_id, $topic_id );
			}
		}

		$updated_id = wp_update_post( array(
			'ID' => $topic_id,
			'post_status' => 'publish',
		) );

		if ( is_wp_error( $updated_id ) )
			trigger_error( 'Failed to publish topic: ' . $topic_id->get_error_message(), E_USER_WARNING );
		return $updated_id;
	}

	public function submission_address( $user ) {
		$reply_key = new ES_Reply_Key( $this->_id, $user );
		if ( EasySubscribe::$options->get( 'readable_address' ) ) {
			$readable_address = new ES_Readable_Address( $reply_key, ES_Post_Types::GROUP );
			$email = $readable_address->get_address();
		} else {
			$email = EasySubscribe::get_reply_address( $this->get_post(), $user );
		}
		return $email;
	}
}