<?php

/**
 * Class ES_Email
 */
class ES_Email {
	public $to;
	public $subject;
	public $message;
	public $headers;
	public $from_name;
	public $from_email;
	public $reply_name;
	public $reply_email;
	public $attachments;
	public $content_type;
	public $template;

	private $string_attachments = array();

	static public function full_address( $email = null, $name = '' ) {
		if ( !$email )
			return self::default_from_email();

		if ( empty( $name ) )
			return $email;

		return $name . ' <' . $email . '>';
	}

	static public function default_from_email() {
		$domain = strtolower( $_SERVER['SERVER_NAME'] );
		if ( substr( $domain, 0, 4 ) == 'www.' ) {
			$domain = substr( $domain, 4 );
		}

		return apply_filters( 'es_default_from_email', 'easysubscribe@' . $domain );
	}

	public function __construct( $values = '' ) {
		$defaults = array(
			'to' => '',
			'subject' => '[' . get_option( 'blogname' ) . '] ' . __( 'notification', 'EasySubscribe' ),
			'message' => '',
			'headers' => array(),
			'from_name' => get_option( 'blogname' ),
			'from_email' =>  self::default_from_email(),
			'reply_name' => '',
			'reply_email' => '',
			'attachments' => array(),
			'content_type' => 'text/html',
			'template' => 'html-email-wrapper.php'
		);
		$values = wp_parse_args( $values, $defaults );
		foreach( $values as $name => $value ) {
			$this->$name = $value;
		}
	}

	public function get_content_type() {
		return $this->content_type;
	}

	public function add_string_attachment( $string, $filename, $encoding = 'base64' , $type = 'application/octet-stream' ) {
		$this->string_attachments[] = compact( 'string', 'filename', 'encoding', 'type' );
	}

	public function send() {
		if ( !is_array( $this->headers ) )
			$this->headers = (array) $this->headers;

		$this->headers[] = 'from: ' . self::full_address( $this->from_email, $this->from_name );

		if ( !empty( $this->reply_email ) )
			$this->headers[] = 'reply-to: ' . self::full_address( $this->reply_email, $this->reply_name );

		if ( !empty( $this->template ) ) {
			// Wrap the message in the given template
			$template = EasySubscribe::locate_template( $this->template );
			$template_data = array(
				'subject' => $this->subject,
				'message' => $this->message,
			);
			$this->message = EasySubscribe::render_template( $template, $template_data, false );
		}

		// About changing content type: http://core.trac.wordpress.org/ticket/23578
		add_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );

		if ( empty( $this->string_attachments ) ) {
			$sent = wp_mail( $this->to, $this->subject, $this->message, $this->headers, $this->attachments );
		} else {
			$string_attachments = $this->string_attachments;
			$phpmailer_action = function( $phpmailer ) use ( $string_attachments ) {
				foreach( $string_attachments as $string_attachment ) {
					call_user_func_array( array( $phpmailer, 'AddStringAttachment' ), $string_attachment );
				}
			};
			add_action( 'phpmailer_init', $phpmailer_action );
			$sent = wp_mail( $this->to, $this->subject, $this->message, $this->headers, $this->attachments );
			remove_action( 'phpmailer_init', $phpmailer_action );
		}

		remove_filter( 'wp_mail_content_type', array( $this, 'get_content_type' ) );
		return $sent;
	}
}