<?php

class ES_Options_Page extends scbAdminPage {

	protected $_overridden_options;

	function __construct( $file = false, $options = null, $overrides = null ) {
		parent::__construct( $file, $options );
		$this->_overridden_options = $overrides;
	}

	function setUp() {
		$this->args = array(
			'page_title' => 'Easy Subscribe',
		);
	}

	function page_content() {
		$table_entries = array(
			array(
				'title' => __( 'Auto subscribe authors', 'EasySubscribe' ),
				'type' => 'checkbox',
				'name' => 'auto_subscribe_authors',
				'desc' => __( 'subscribe authors to comments on their posts', 'EasySubscribe' ),
			),
			array(
				'title' => __( 'Reply by email', 'EasySubscribe' ),
				'type' => 'checkbox',
				'name' => 'reply_by_email',
				'desc' => __( 'enable', 'EasySubscribe' ),
			),
			array(
				'title' => __( 'Reply by email user', 'EasySubscribe' ),
				'type' => 'text',
				'name' => 'reply_by_email_user',
				'desc' => __( 'Email service user (supplied by vernalcreative.com)', 'EasySubscribe' ),
			),
			array(
				'title' => __( 'Enable groups', 'EasySubscribe' ),
				'type' => 'checkbox',
				'name' => 'enable_groups',
				'desc' => __( 'add post types and email handling for group conversations', 'EasySubscribe' ),
			),
		);
		if ( $this->options->get( 'enable_groups' ) or !empty( $this->_overridden_options['enable_groups'] ) ) {
			$table_entries[] = array(
				'title' => __( 'Readable group address', 'EasySubscribe' ),
				'type' => 'checkbox',
				'name' => 'readable_address',
				'desc' => __( 'this requires a customized readable reply by email user also', 'EasySubscribe' ),
			);
			$table_entries[] = array(
				'title' => __( 'Auto subscribe topic comments', 'EasySubscribe' ),
				'type' => 'checkbox',
				'name' => 'auto_subscribe_group_comments',
				'desc' => __( 'send comments (as well as topics) to all group members', 'EasySubscribe' ),
			);
		}
		foreach ( $table_entries as $index => $entry ) {
			if ( isset( $this->_overridden_options[$entry['name']] ) ) {
				$table_entries[$index]['extra'] = array(
					'class' => 'overridden',
					'disabled' => 'disabled',
				);
			}
		}

		echo $this->form_table( $table_entries, $this->options->get() );
	}

	function validate( $new_data, $old_data ) {
		$checkbox_fields = array( 'reply_by_email', 'auto_subscribe_authors', 'enable_groups' );
		$checkbox_fields = array_diff( $checkbox_fields, array_keys( $this->_overridden_options ) );
		$valid_data = $old_data;
		foreach ( $checkbox_fields as $field ) {
			if ( isset( $new_data[$field] ) )
				$valid_data[$field] = true;
			else
				$valid_data[$field] = false;
		}
		if ( isset( $new_data['reply_by_email_user'] ) )
			$valid_data['reply_by_email_user'] = sanitize_key( $new_data['reply_by_email_user'] );

		if ( !empty( $valid_data['reply_by_email'] ) and empty( $valid_data['reply_by_email_user'] ) )
			add_settings_error( 'reply_by_email_user', 'user_required', __( 'The email user is required.', 'EasySubscribe' ) );

		return $valid_data;
	}

}