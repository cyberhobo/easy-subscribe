<?php

/**
 * An experimental attempt to make a more readable personal posting email address.
 *
 * @todo Determine whether the readable address is too vulnerable to spamming.
 *
 */
class ES_Readable_Address {
	/** @var string */
	private $_address = null;
	/** @var ES_Reply_Key */
	private $_reply_key = null;
	/** @var string  */
	private $_post_type = ES_Post_Types::GROUP;

	public function __construct( $address_or_reply_key, $post_type = null ) {
		if ( is_object( $address_or_reply_key ) ) {
			$this->_reply_key = $address_or_reply_key;
		} else {
			$this->_address = $address_or_reply_key;
		}

		if ( !is_null( $post_type ) )
			$this->_post_type = $post_type;

	}

	public function get_address() {
		if ( is_null( $this->_address ) ) {
			$this->_address = EasySubscribe::$options->get( 'reply_by_email_user' ) . '-' .
				$this->_reply_key->post->post_name . '-' .
				$this->create_token() . '@' . EasySubscribe::$options->get( 'reply_by_email_domain' );
		}
		return $this->_address;
	}

	public function get_reply_key() {
		if ( is_null( $this->_reply_key ) ) {
			$this->parse_address();
		}
		return $this->_reply_key;
	}

	/**
	 * An experiment to store a user's reply key for a group indexed by a 5 character token.
	 *
	 * @todo Finalize the token length
	 *
	 * @return string
	 */
	private function create_token() {
		$full_code = $this->_reply_key->get_code();
		$start = 0;
		do {
			$token = substr( $full_code, $start, 5 );
			$existing_code = get_post_meta( $this->_reply_key->get_post_id(), '_' . $token, true );
			$start++;
		} while ( $existing_code and $existing_code != $full_code );
		if ( !$existing_code )
			update_post_meta( $this->_reply_key->get_post_id(), '_' . $token, $full_code );
		return $token;
	}

	/**
	 * Find the post and user for an address and create the reply key from them.
	 * @return bool
	 */
	private function parse_address() {
		$prefix = EasySubscribe::$options->get( 'reply_by_email_user' ) . '-';

		if ( strpos( $this->_address, $prefix ) != 0 )
			return false;

		list( $user_part, $domain_part ) = explode( '@', $this->_address );
		if ( '-' != substr( $user_part, -6, 1 ) )
			return false;

		$token = substr( $user_part, -5 );
		$slug = substr( $user_part, strlen( $prefix ), -6 );
		$post = get_page_by_path( $slug, OBJECT, $this->_post_type );

		if ( !$post ) {
			trigger_error( __( 'Found no post for readable address', 'EasySubscribe' ) . ': ' . $user_part, E_USER_WARNING );
			return false;
		}

		$code = get_post_meta( $post->ID, '_' . $token, true );
		if ( !$code ) {
			$msg = sprintf( __( 'Invalid user token %s for post %s', 'EasySubscribe' ), $token, $post->ID );
			trigger_error( $msg, E_USER_WARNING );
			return false;
		}

		$this->_reply_key = new ES_Reply_Key( $code );
		return true;
	}
}