<?php

/**
 * User-centric behavior
 *
 * Static methods are generally hooks.
 *
 * Instantiation augments a WP_User.
 */
class ES_User {
	const SUBSCRIBED_TERM_META_KEY = 'es_subscribed_term';

	/** @var  int user ID */
	private $_id;
	/** @var WP_User user object */
	private $_user;

	/**
	 * Create an EasySubscribe user.
	 *
	 * @param int|WP_User $user_id_or_object
	 */
	public function __construct( $user_id_or_object ) {
		if ( is_a( $user_id_or_object, 'WP_User' ) ) {
			$this->_user = $user_id_or_object;
			$this->_id = $this->_user->ID;
		} else {
			$this->_id = intval( $user_id_or_object );
		}
	}

	/**
	 * When a user is deleted, remove their subscriptions.
	 *
	 * Hooks deleted_user.
	 * @param $id
	 */
	public static function action_deleted_user( $id ) {
		$es_user = new ES_User( $id );
		$subscribed_posts = $es_user->get_subscribed_posts();
		foreach ( $subscribed_posts as $post ) {
			$es_user->unsubscribe( 'post', $post );
		}
		$subscribed_author_ids = $es_user->get_subscribed_author_ids( $id );
		foreach ( $subscribed_author_ids as $author_id ) {
			$es_user->unsubscribe( 'user', $author_id );
		}
	}

	/**
	 * Echo the term form for a user.
	 * @param WP_User $user
	 */
	public static function echo_term_form( $user ) {
		$taxonomies = EasySubscribe::$options->get( 'enable_taxonomies' );
		if ( empty( $taxonomies ) )
			return;
		$es_user = new ES_User( $user );
		$subscribed_term_signatures = $es_user->get_subscribed_term_signatures();

		$template = EasySubscribe::locate_template( 'profile-term-form.php' );
		$template_data = compact( 'taxonomies', 'user', 'subscribed_term_signatures' );
		EasySubscribe::render_template( $template, $template_data );
	}

	/**
	 * When a profile is saved, also save changes to term subscriptions.
	 *
	 * Hooks edit_user_profile_updated and profile_options_updated.
	 * @param $user_id
	 */
	public static function process_term_form( $user_id ) {
		$taxonomies = EasySubscribe::$options->get( 'enable_taxonomies' );
		if ( empty( $taxonomies ) )
			return;

		$es_user = new ES_User( $user_id );
		$posted_sigs = isset( $_POST[self::SUBSCRIBED_TERM_META_KEY] ) ? $_POST[self::SUBSCRIBED_TERM_META_KEY] : array();
		$subscribed_sigs = $es_user->get_subscribed_term_signatures();

		$additions = array_diff( $posted_sigs, $subscribed_sigs );
		foreach( $additions as $added_sig ) {
			add_user_meta( $user_id, self::SUBSCRIBED_TERM_META_KEY, $added_sig );
		}

		$subtractions = array_diff( $subscribed_sigs, $posted_sigs );
		foreach( $subtractions as $removed_sig ) {
			delete_user_meta( $user_id, self::SUBSCRIBED_TERM_META_KEY, $removed_sig );
		}
	}

	/**
	 * Get the underlying user.
	 * @return null|WP_User
	 */
	public function get_user() {
		if ( !isset( $this->_user ) )
			$this->_user = get_userdata( $this->_id );
		return $this->_user;
	}

	/**
	 * Ensure that the user is subscribed to an object.
	 *
	 * Does nothing if the user is already subscribed.
	 *
	 * @param $type
	 * @param $object
	 * @return bool
	 */
	public function ensure_subscribed( $type, $object ) {
		$result = false;
		$object_id = is_object( $object ) ? $object->ID : intval( $object );
		if ( !$object_id )
			return $result;

		$subscriber_ids = EasySubscribe::get_subscriber_ids( $type, $object_id );

		if ( !in_array( $this->_id, $subscriber_ids ) ) {
			array_push( $subscriber_ids, $this->_id );
			$result = update_metadata( $type, $object_id, EasySubscribe::SUBSCRIBED_META_KEY, $subscriber_ids );
			if ( $result )
				do_action( 'es_subscribed', $type, $this->_id, $object_id );
		}
		return $result;
	}

	/**
	 * Unsubscribe a user from an object.
	 *
	 * @param string $type post or user
	 * @param object|int $object object or ID to unsubscribe from.
	 * @return bool True if the user has been unsubscribed.
	 */
	public function unsubscribe( $type, $object ) {
		$object_id = is_object( $object ) ? $object->ID : intval( $object );
		$success = true;

		$subscriber_ids = EasySubscribe::get_subscriber_ids( $type, $object_id );
		if ( in_array( $this->_id, $subscriber_ids ) ) {
			$subscriber_ids = array_diff( $subscriber_ids, array( $this->_id ) );
			$success = update_metadata( $type, $object_id, EasySubscribe::SUBSCRIBED_META_KEY, $subscriber_ids );
			if ( $success )
				do_action( 'es_unsubscribed', $type, $this->_id, $object_id );
		}

		return $success;
	}

	/**
	 * Determine whether a user is subscribed to an object.
	 *
	 * @param string $type object or user
	 * @param int|object $object
	 * @return bool True if the user is subscribed to the object.
	 */
	public function is_subscribed( $type, $object ) {
		$object_id = is_object( $object ) ? $object->ID : intval( $object );
		if ( !$object_id )
			return false;

		$subscriber_ids = EasySubscribe::get_subscriber_ids( $type, $object_id );

		return in_array( $this->_id, $subscriber_ids );
	}

	/**
	 * Get the posts the user is subscribed to.
	 *
	 * @return array posts
	 */
	public function get_subscribed_posts() {
		// Here we decide how to deal with querying serialized data
		// Choosing to depend on PHP's serial format
		$posts = get_posts( array(
			'post_type' => 'any',
			'posts_per_page' => -1,
			'meta_query' => array(
				array(
					'key' => EasySubscribe::SUBSCRIBED_META_KEY,
					'value' => 'i:' . $this->_id . ';',
					'compare' => 'LIKE',
				)
			)
		) );
		return $posts;
	}

	/**
	 * Get the post IDs the user is subscribed to.
	 *
	 * @return array author IDs
	 */
	public function get_subscribed_author_ids() {
		global $wpdb;

		// Here we decide how to deal with querying serialized data
		// Choosing to depend on PHP's serial format
		$sql = "SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key='" . EasySubscribe::SUBSCRIBED_META_KEY .
			"' and meta_value LIKE '%i:" . $this->_id . ";%'";

		return $wpdb->get_col( $sql );
	}

	/**
	 * @return array Signatures are taxonomy+term_id.
	 */
	public function get_subscribed_term_signatures() {
		return get_user_meta( $this->_id, self::SUBSCRIBED_TERM_META_KEY );
	}
}