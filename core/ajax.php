<?php

/**
 * Handle Ajax Requests
 */
class ES_Ajax {
	const AJAX_NONCE = 'es_subscribe';

	/**
	 * Handle subscription ajax requests.
	 */
	static public function action_wp_ajax_es_subscribe() {

		// Validate request
		if ( !wp_verify_nonce( $_POST['subscribe_nonce'], self::AJAX_NONCE ) )
			wp_die( -1 );

		$subscriber = wp_get_current_user();

		$type = sanitize_key( $_POST['meta_type'] );

		$object_id = intval( $_POST['object_id'] );

		$success = '';
		$new_user_notification_sent = false;

		if ( empty( $subscriber->ID ) and isset( $_POST['subscribe_email'] ) ) {

			$email = is_email( $_POST['subscribe_email'] );

			if ( !$email )
				wp_die( '<div class="error">' . __( 'Sorry, that email address is not valid.', 'EasySubscribe' ) . '</div>' );

			$subscriber = get_user_by( 'email', $email );

			if ( empty( $subscriber->ID ) ) {
				$password = wp_generate_password();

				$suffix = '';
				$basename = substr( $email, 0, strpos( $email, '@' ) );
				do {
					$username = $basename . $suffix;
					$suffix++;
				} while ( username_exists( $username ) );

				$user_id = wp_create_user( $username, $password, $email );

				if ( is_wp_error( $user_id ) )
					wp_die( $user_id->get_error_message() );

				$subscriber = get_user_by( 'id', $user_id );

				$send_notification = apply_filters( 'es_send_new_user_notification', true );

				if ( $send_notification ) {
					$success = __( 'Account created. You\'ll receive an email with more information.', 'EasySubscribe' ) . ' ';
					$success = apply_filters( 'es_subscribed_message', $success, $subscriber, $send_notification );
					$template = EasySubscribe::locate_template( 'new-user-email.php' );
					ES_Outbound_Handling::send_new_user_notification( $subscriber, $password, $template );
					$new_user_notification_sent = true;
				}
			}
		}

		if ( !$object_id )
			wp_die( sprintf( __( 'Sorry, %s ID %s was not found.', 'EasySubscribe' ), $type, $object_id ) );

		$subscriber_ids = EasySubscribe::get_subscriber_ids( $type, $object_id );

		if ( in_array( $subscriber->ID, $subscriber_ids ) ) {

			if ( !EasySubscribe::unsubscribe( $type, $subscriber->ID, $object_id ) )
				wp_die( sprintf( __( 'Sorry, failed to unsubscribe %s from %s %s.', 'EasySubscribe' ), $subscriber->ID, $type, $object_id ) );

			ES_Outbound_Handling::send_subscription_notification( $type, $subscriber, $object_id, true );

			$success .= __( 'Unsubscribed.', 'EasySubscribe' );

		} else {

			if ( !EasySubscribe::ensure_subscribed( $type, $subscriber->ID, $object_id ) )
				wp_die( sprintf( __( 'Sorry, failed to subscribe %s to %s %s.', 'EasySubscribe' ), $subscriber->ID, $type, $object_id ) );

			$success .= __( 'You have successfully subscribed.', 'EasySubscribe' );

			if ( ES_Outbound_Handling::send_subscription_notification( $type, $subscriber, $object_id, false ) and !$new_user_notification_sent )
				$success .= ' ' . __( 'Please check your email for further information.', 'EasySubscribe' );

		}

		echo $success;

		wp_die();
	}

}