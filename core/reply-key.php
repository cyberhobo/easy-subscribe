<?php
/**
 * Email reply key management.
 * @package Namati
 */

class ES_Reply_Key {

	private $user;
	private $user_id;
	private $post;
	private $post_id;
	private $hash;
	private $code;
	private $errors = array();

	/**
	 * Construct a key for a post and user.
	 * @param object|int|string $post_or_code A post ID, object, or an encoded key.
	 * @param object|int $user If encoding, the user or user ID.
	 */
	public function __construct( $post_or_code, $user = null ) {

		if ( is_object( $post_or_code ) ) {

			$this->post_id = $post_or_code->ID;
			$this->post = $post_or_code;

		} else if ( is_numeric( $post_or_code ) ) {

			$this->post_id = intval( $post_or_code );

		} else {

			$this->code = $post_or_code;

		}

		if ( is_object( $user ) ) {

			$this->user_id = $user->ID;
			$this->user = $user;

		} else if ( $user ) {

			$this->user_id = intval( $user );
		}

	}

	public function __get( $name ) {
		return call_user_func( array( $this, 'get_' . $name ) );
	}

	public function get_user() {
		if ( !isset( $this->user ) )
			$this->user = get_userdata( $this->get_user_id() );

		return $this->user;
	}

	public function get_user_id() {
		if ( !isset( $this->user_id ) and isset( $this->code ) )
			$this->decode();

		return $this->user_id;
	}

	public function get_post() {
		if ( !isset( $this->post ) )
			$this->post = get_post( $this->get_post_id() );

		return $this->post;
	}

	public function get_post_id() {
		if ( !isset( $this->post_id ) )
			$this->decode();
		return $this->post_id;
	}

	public function get_code() {
		if ( !isset( $this->code ) )
			$this->encode();
		return $this->code;
	}

	private function encode() {

		if ( !$this->post_id or !$this->user_id ) {
			trigger_error( __( 'Not encoding empty post or user id', 'EasySubscribe' ), E_USER_WARNING );
			return;
		}

		if ( !isset( $this->hash ) )
			$this->hash = md5( $this->post_id . $this->user_id . wp_salt( __CLASS__ ) );

		$this->code = base64_encode( $this->post_id . ' ' . $this->user_id . ' ' . $this->hash );
	}

	private function decode() {
		list( $post_id, $user_id, $hash ) = explode( ' ', base64_decode( $this->code ) );

		if ( !$post_id or !$user_id or !$hash ) {
			trigger_error( __( 'Code missing post id, user id, or hash', 'EasySubscribe' ) . ': ' . $this->code, E_USER_WARNING );
			return;
		}

		$true_hash = md5( $post_id . $user_id . wp_salt( __CLASS__ ) );

		if ( $hash != $true_hash ) {
			trigger_error( __( 'Wrong hash, not decoding reply key', 'EasySubscribe' ), E_USER_WARNING );
			return;
		}

		$this->user_id = $user_id;
		$this->post_id = $post_id;
		$this->hash = $hash;
	}
}