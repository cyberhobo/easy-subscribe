<?php

/**
 * Class ES_Groups The Easy Subscribe groups module.
 */
class ES_Groups {
	/**
	 * Load dependencies and resources required for the current request.
	 */
	static public function load() {
		require_once EasySubscribe::$dir_path . '/post-types/' . ES_Post_Types::GROUP . '.php';
		require_once EasySubscribe::$dir_path . '/post-types/' . ES_Post_Types::TOPIC . '.php';
	}

}